﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartitionedTextureClicking : MonoBehaviour {

	public Camera cam;
	public TexturePartition textPartition;
	public Transform drawingTarget;
	public float offsetTarget;

	public bool selectionEnabled;

	private PanningZoomingCamera panZoomComponent;
	private LevelManager levelManager;
	private SpawningPlayer spawningPlayer;
	private MyHistogram myHistogram;

	private bool dirty = false;
	public static Color nullColor = new Color(-1f, -1f, -1f, -1f);
	private Color currClusterColor = nullColor;

	private Queue<Vector2> nodesFilled;
	private Texture2D currentTexture;

	private bool myFloodFilling = false;

	void Start()
	{
		panZoomComponent = cam.GetComponent<PanningZoomingCamera> ();
		levelManager = GetComponent<LevelManager> ();
		spawningPlayer = GetComponent<SpawningPlayer> ();
		myHistogram = GetComponent<MyHistogram> ();

		nodesFilled = new Queue<Vector2>();
	}

	void Update()
	{
		if (myFloodFilling) {
		
			Vector2 next = nodesFilled.Dequeue ();
			currentTexture.SetPixel ((int)next.x, (int)next.y, Color.green);

			if (nodesFilled.Count == 0)
				myFloodFilling = false;
			else {
			
				next = nodesFilled.Dequeue ();
				currentTexture.SetPixel ((int)next.x, (int)next.y, Color.green);

				if (nodesFilled.Count == 0)
					myFloodFilling = false;
			}

			currentTexture.Apply ();

			return;
		}

	}
		
	public void StartColoringProcedure(bool definitive, Vector2 XYpos)
	{
		RaycastHit visorPlaneHit = new RaycastHit();
		Color planeColor = new Color();

		Vector3 offsetPosition = new Vector3 (XYpos.x, XYpos.y + offsetTarget, Input.mousePosition.z);

		RaycastHit[] hits;
		hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(offsetPosition), 100.0F);

		for (int i = 0; i < hits.Length; i++) 
		{
			RaycastHit hitto = hits[i];

			if (hitto.collider.tag == "PartitionedPlane") {}

			if (hitto.collider.tag == "BiasPlane") {

				if (definitive) {
				
					ColoringOnBias (hitto, definitive);

				} else {

					PreviewOnBias (hitto, definitive);
				}
			}
		}
	}

	void ColoringOnBias(RaycastHit planeHit, bool definitive)
	{
		Renderer rend = planeHit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = planeHit.collider as MeshCollider;

		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
			return;

		Texture2D biasTex = rend.material.mainTexture as Texture2D;

		Vector2 pixelUV = planeHit.textureCoord;

		pixelUV.x *= biasTex.width;
		pixelUV.y *= biasTex.height;

		if (!biasTex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.black)
		    && !biasTex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.clear)) {
		
			var pixelX = (int)pixelUV.x;
			var pixelY = (int)pixelUV.y;
						
			Texture2D superpiTex = textPartition.GetSuperpixelTexture ();
			Texture2D clusTex = textPartition.GetClusteredTexture ();
			Texture2D visorTex = textPartition.GetVisorTexture ();

			textPartition.ResetBackupsTexture ();

			superpiTex.FloodFillPermanent(pixelX, pixelY, biasTex, visorTex, clusTex, levelManager);

			biasTex.Apply ();
			visorTex.Apply ();

			textPartition.UpdateBackupTextures ();

			biasTex.Apply ();
			visorTex.Apply ();

		}
	}

	void PreviewOnBias(RaycastHit planeHit, bool definitive)
	{
		Renderer rend = planeHit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = planeHit.collider as MeshCollider;

		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
			return;

		Texture2D biasTex = rend.material.mainTexture as Texture2D;

		Vector2 pixelUV = planeHit.textureCoord;

		pixelUV.x *= biasTex.width;
		pixelUV.y *= biasTex.height;

		if (!biasTex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.black)
			&& !biasTex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.clear)) {

			var pixelX = (int)pixelUV.x;
			var pixelY = (int)pixelUV.y;

			if (!IsSameSuperPixel (pixelX, pixelY)) {
			
				textPartition.ResetBackupsTexture ();
			}

			Texture2D superpiTex = textPartition.GetSuperpixelTexture();
			Texture2D visorTex = textPartition.GetVisorTexture ();

			superpiTex.FloodFillPreview(pixelX, pixelY, biasTex, visorTex);
			biasTex.Apply ();
			visorTex.Apply ();

			//	nodesFilled = tex.FloodFillAreaAnimated(pixelX, pixelY, Color.green);
			//	currentTexture = tex;
			//	floodFilling = true;

			
		}

	}
		
	public void ResettingPlaneTextures()
	{
		textPartition.ResetBackupsTexture ();
		textPartition.ApplyTextureChanges ();
	}

	bool IsSameSuperPixel(int pixelX, int pixelY)
	{
		Color nowCluster = textPartition.GetSuperPixel(pixelX, pixelY);
	
		if (currClusterColor.Equals(nullColor)) {
		
			currClusterColor = nowCluster;

			return true;

		} else {
			
			if (nowCluster.Equals (currClusterColor)) {

				currClusterColor = nowCluster;
				return true;

			} else {
			
				currClusterColor = nowCluster;
				return false;
			}

		}
	}

	public void ResetClusterVariables()
	{
		dirty = false;
		currClusterColor = nullColor;
	}

	public void MovePartitionedPlane()
	{
		transform.position = new Vector3 (transform.position.x + 70f, transform.position.y, transform.position.z);
	}
}
