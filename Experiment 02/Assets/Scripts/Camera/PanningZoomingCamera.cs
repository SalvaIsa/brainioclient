﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PanningZoomingCamera : MonoBehaviour {

	public Transform player;
	public GameSceneManagement sceneManagement;
	public Text debugText;

	public Button zawarudoButton;
	public Button selectionButton;

	//*************		 Private Variables
	private Camera mainCamera;
	private Vector3 startingCameraPosition;
	private bool zoomingActive;

	private float startingFoV;
	private readonly float zoomingFoV = 17;

	private Color zawarudoColor = new Color (195f/255f, 74f/255f, 74f/255f);
	private Color selectionColor = new Color (74f/255f, 80f/255f, 195f/255f);
	private bool zawarudoModeActive;
	private bool selectionModeActive;

	private bool movingCompleted;

	// ************* 	Zoom IN - OUT Handling
	private static float[] _zoomBounds;
	private static readonly float _zoomSpeedMouse = 5.5f;

	private static readonly float _zoomSpeedTouch = 0.1f;
	private bool wasZoomingLastFrame; 
	private Vector2[] lastZoomPositions;

	void Start()
	{
		mainCamera = GetComponent<Camera>();
		startingCameraPosition = mainCamera.transform.position;
		startingFoV = mainCamera.fieldOfView;

		movingCompleted = true;

		_zoomBounds = new float[]{zoomingFoV, startingFoV};
	}

	void Update()
	{
		if (zoomingActive) {
			
			if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {

				if (!SpawningPlayer.IsPointerOverUIObject()) {
					HandleTouch ();
				}

			} else {

				if (!SpawningPlayer.IsPointerOverUIObject ()) {

					HandleMouse ();
				}
			}
		}
	}

	private void HandleTouch()
	{
		switch(Input.touchCount) {

		case 2: // Zooming

			Vector2[] newPositions = new Vector2[]{Input.GetTouch(0).position, Input.GetTouch(1).position};
			if (!wasZoomingLastFrame) {
				lastZoomPositions = newPositions;
				wasZoomingLastFrame = true;
			} else {
				// Zoom based on the distance between the new positions compared to the 
				// distance between the previous positions.
				float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
				float oldDistance = Vector2.Distance(lastZoomPositions[0], lastZoomPositions[1]);
				float offset = newDistance - oldDistance;

				debugText.text = offset + "";

				//	ZoomCamera(offset, _zoomSpeedTouch);
				if (offset < 0) {
					sceneManagement.MoveZoomCamera (startingCameraPosition,	startingFoV);
					zoomingActive = false;
				}

				lastZoomPositions = newPositions;
			}


			break;

		default: 

			wasZoomingLastFrame = false;
			break;

		}
	}

	private void HandleMouse()
	{
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		debugText.text = scroll + "";

		if (scroll < 0) {
			sceneManagement.MoveZoomCamera (startingCameraPosition,	startingFoV);
			zoomingActive = false;
		}

		//ZoomCamera(scroll, _zoomSpeedMouse);
	}

	void ZoomCamera(float offset, float speed) {
		if (offset == 0) {
			return;
		}

		mainCamera.fieldOfView = Mathf.Clamp(mainCamera.fieldOfView - (offset * speed), _zoomBounds[0], _zoomBounds[1]);
	}

	public void MoveToNextSlice()
	{
		movingCompleted = false;
		transform.DOMoveX (70f, 0.5f).SetRelative ().OnComplete(()=> movingCompleted = true);

		Vector3 newPos = new Vector3 (startingCameraPosition.x + 70f, startingCameraPosition.y, startingCameraPosition.z);
		startingCameraPosition = newPos;
	}

	public bool IsMovingCompleted()
	{
		return movingCompleted;
	}

	public void ZoomInPlayer()
	{
		Vector3 newCamPos = new Vector3 (player.position.x, player.position.y, mainCamera.transform.position.z);
		mainCamera.transform.position = newCamPos;
		mainCamera.fieldOfView = zoomingFoV;
		zoomingActive = true;
	}
		
	public void PlayModeCamera()
	{
		mainCamera.transform.position = startingCameraPosition;
		mainCamera.fieldOfView = startingFoV;
		zoomingActive = false;
	}
				
	public void SetSelectionCamPos(Vector3 pos)
	{
		mainCamera.transform.position = startingCameraPosition;
	}

	public void SetCurrentCamPos(Vector3 pos)
	{
		Vector3 newPos = new Vector3 (pos.x, pos.y + 2, startingCameraPosition.z);

		mainCamera.transform.position = newPos;
	}

	public void SetStartingCamPos(Vector3 pos)
	{
		Vector3 newPos = new Vector3 (pos.x, pos.y + 2, startingCameraPosition.z);
		
		startingCameraPosition = newPos;
	}

	public float GetZoomingFov()
	{
		return zoomingFoV;	
	}

	public void SetZoomingIn(bool value)
	{
		zoomingActive = true;
	}

	public bool IsZoomingIn()
	{
		return zoomingActive;	
	}
		
	public bool isZawarudoActive()
	{
		return zawarudoModeActive;
	}

	public bool isSelectionActive()
	{
		return selectionModeActive;
	}
}
