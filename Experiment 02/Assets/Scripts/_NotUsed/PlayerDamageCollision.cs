﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamageCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnCollisionEnter(Collision other)
	{
		DamageBehaviour dmg = other.gameObject.GetComponent<DamageBehaviour> ();

		if (dmg != null) {
		
			if(Input.GetKey(KeyCode.O))
				dmg.SelectDamage ();
		}
		else
			Debug.Log ("collido di meno");
	}
}
