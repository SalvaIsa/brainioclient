﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlayer : MonoBehaviour {


	public float maxSpeed;
	public float minSpeed;

	public SimpleTouchController leftController;
	private float speed;

	private Rigidbody2D _rb2d;

	//**************
	public Camera cam;
	public Transform playerTarget;

	void Start () {

		//_rb2d = GetComponent<Rigidbody2D> ();
		//this.speed = maxSpeed;
	
	}

	void Update() {

		if (!Input.GetMouseButton(0))
			return;

		RaycastHit hit;
		if (!Physics.Raycast (cam.ScreenPointToRay (Input.mousePosition), out hit))
			return;

		if (hit.collider != null) {
		
		}

		Vector3 newPos = new Vector3 (hit.point.x, hit.point.y, playerTarget.position.z);
		playerTarget.position = newPos;

		if (!playerTarget.gameObject.activeSelf)
			playerTarget.gameObject.SetActive (true);
	}
		
	void FixedUpdate () {
	
		/*
		Vector2 targetVelocity = new Vector2(leftController.GetTouchPosition.x, leftController.GetTouchPosition.y);
		GetComponent<Rigidbody2D>().velocity = targetVelocity * speed;
		*/
	}

	void HandleTouch()
	{
		
	}

	void HandleMouse()
	{
		if (Input.GetMouseButtonDown(0)) {
			transform.position = Input.mousePosition;
		}
	}

	public void playerLaserOn()
	{
		this.speed = minSpeed;
	}

	public void playerLaserOff()
	{
		this.speed = maxSpeed;
	}

}