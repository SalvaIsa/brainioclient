﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScottPlayer : MonoBehaviour {

	private CharacterController controller;

	private Vector3 moveDirection;
	private Vector3 forward;
	private Vector3 right;

	void Start () {

		controller = GetComponent<CharacterController> ();

		moveDirection = Vector3.zero;
		forward = Vector3.zero;
		right = Vector3.zero;
	}
	
	void Update () {

		forward = transform.forward;
		right = new Vector3 (forward.z, 0.0f, -forward.x);

		float horizontalInput = Input.GetAxisRaw ("Horizontal");
		float verticalInput = Input.GetAxisRaw ("Vertical");

		Vector3 targetDirection = horizontalInput * right + verticalInput * forward;

		moveDirection = Vector3.RotateTowards (moveDirection, targetDirection, 200 * Mathf.Deg2Rad, 1000);

		controller.Move (moveDirection * Time.deltaTime * 2);

		if(targetDirection != Vector3.zero)
		{
			transform.rotation = Quaternion.LookRotation (moveDirection);
		}

	}
}
