﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SykooPlayer : MonoBehaviour {

	public float movementeSpeed;
	public float rotationSpeed;

	public GameObject playerObj;

	public GameObject bulletSpawnPoint;
	public float waitTime;
	public GameObject bullet;

	private Rigidbody rigBody;

	void Start()
	{
		rigBody = GetComponent<Rigidbody> ();
	}

	void Update()
	{
		// Facing mouse Player
		/*
		Plane playerPlane = new Plane (Vector3.up, transform.position);
		Ray ray = UnityEngine.Camera.main.ScreenPointToRay (Input.mousePosition);
		float hitDist = 0.0f;

		if (playerPlane.Raycast (ray, out hitDist)) {
		
			Vector3 targetPoint = ray.GetPoint (hitDist);
			Quaternion targetRotation = Quaternion.LookRotation (targetPoint - transform.position);
			targetRotation.x = 0;
			targetRotation.z = 0;
			playerObj.transform.rotation = Quaternion.Slerp (playerObj.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
		}
		*/
		//Move();

		// Shootinh Player
		if (Input.GetMouseButtonDown (0)) {
		
			Shoot ();
		}
	}

	void FixedUpdate()
	{
		BetterMove ();
	}

	private void Move()
	{
		if (Input.GetKey (KeyCode.W))
			transform.Translate (Vector3.forward * movementeSpeed * Time.deltaTime);	

		if (Input.GetKey (KeyCode.A))
			transform.Translate (Vector3.left * movementeSpeed * Time.deltaTime);	

		if (Input.GetKey (KeyCode.S))
			transform.Translate (Vector3.back * movementeSpeed * Time.deltaTime);	

		if (Input.GetKey (KeyCode.D))
			transform.Translate (Vector3.right * movementeSpeed * Time.deltaTime);	
	}

	private void BetterMove()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rigBody.AddForce (movement * movementeSpeed);
	}

	private void Shoot()
	{
		Instantiate (bullet.transform, bulletSpawnPoint.transform.position, Quaternion.identity);
	}


}
