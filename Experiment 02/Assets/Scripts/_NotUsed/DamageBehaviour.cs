﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DamageBehaviour : MonoBehaviour {

	private int pixelX;
	private int pixelY;
	private float pixelValue;
	public LevelManager levelManager;

	private bool selected;

	void Start () {

		selected = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseOver(){

		if (Input.GetMouseButton (0)) {

			SelectDamage ();
		}
	}

	public void SelectDamage()
	{
		if (!selected) {
			selected = true;

			Renderer renderer = GetComponent<Renderer> ();
			Material mat = renderer.material;

			float emission = 1.0f;
			Color baseColor = Color.red;

			Color finalColor = baseColor * Mathf.LinearToGammaSpace (emission);
				
			mat.SetColor ("_EmissionColor", finalColor);
				
			levelManager.selectedDamges.Add (pixelX + "-" + pixelY);
		}
	}

	public void setDamageColor(float val)
	{
		MeshRenderer ddd = GetComponent<MeshRenderer> ();
		ddd.material.SetColor ("_Color", new Color(val,val,val));
	}

	public void setPixelProperties(int x, int y, float val)
	{
		pixelX = x;
		pixelY = y;
		pixelValue = val;
	}
}
