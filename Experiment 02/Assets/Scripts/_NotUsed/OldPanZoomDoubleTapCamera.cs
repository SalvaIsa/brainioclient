﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OldPanZoomDoubleTapCamera : MonoBehaviour {

	public Transform player;
	public bool enablePanning;

	private Camera mainCamera;
	private Vector3 startingCameraPosition;
	private Vector3 gridmodeCameraPosition = new Vector3 (12.2f, 0f, 24f);
	private bool zoomInActive;
	private bool gridmodeActive;

	private float startingFoV;
	private static float zoomingFoV = 17;
	private static float gridmodeFoV = 110;

	private int currentGrid = 1;
	//***************** Graphical Elements enabled by Zooming in\out
	public Button leftArrow;
	public Button rightArrow;

	//*************** Variables for Panning and Zooming through Mouse\TouchInput 
	private static readonly float PanSpeed = 20f;
	private static readonly float ZoomSpeedTouch = 0.1f;
	private static readonly float ZoomSpeedMouse = 3.5f;

	private static readonly float[] BoundsX = new float[]{-100f, 100f}; 
	private static readonly float[] BoundsZ = new float[]{-100f, 100f};
	private static readonly float[] ZoomBounds = new float[]{10f, 85f};

	private Vector3 lastPanPosition;
	private int panFingerId; // Touch mode only

	private bool wasZoomingLastFrame; // Touch mode only
	private Vector2[] lastZoomPositions; // Touch mode only

	// For double tap detection
	private float touchDuration;
	private Touch touch;

	void Start()
	{
		mainCamera = GetComponent<Camera>();
		startingCameraPosition = mainCamera.transform.position;
		startingFoV = mainCamera.fieldOfView;
	}

	void Update()
	{
		if (gridmodeActive) {

			leftArrow.gameObject.SetActive (true);
			rightArrow.gameObject.SetActive (true);

			int gridLimit;
			if (ApplicationModel.biasTextures == null)
				gridLimit = 2;
			else
				gridLimit = ApplicationModel.biasTextures.Length / 4;

			if (currentGrid == 1) {
				leftArrow.interactable = false;
				rightArrow.interactable = true;
			} else if (currentGrid < gridLimit) {
				leftArrow.interactable = true;
				rightArrow.interactable = true;
			} else {
				leftArrow.interactable = true;
				rightArrow.interactable = false;
			}

		} else {
		
//			leftArrow.gameObject.SetActive (false);
//			rightArrow.gameObject.SetActive (false);
		}


		if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {

			if (!SpawningPlayer.IsPointerOverUIObject()) {
				HandleTouch ();
			}

		} else {

			if (!SpawningPlayer.IsPointerOverUIObject ()) {

				HandleMouse ();
			}
		}

	}

	void CheckDoubleTap()
	{
		if(Input.touchCount > 0){ //if there is any touch
			touchDuration += Time.deltaTime;
			touch = Input.GetTouch(0);

			//	making sure it only check the touch once && 
			//	it was a short touch/tap and not a dragging.
			if(touch.phase == TouchPhase.Ended && touchDuration < 0.2f) 
				StartCoroutine("SingleOrDoubleTap");
		}
		else
			touchDuration = 0.0f;
	}

	void HandleTouch() {
		switch(Input.touchCount) {

		case 1: // Panning

			if (enablePanning) {
				wasZoomingLastFrame = false;

				// If the touch began, capture its position and its finger ID.
				// Otherwise, if the finger ID of the touch doesn't match, skip it.
				Touch touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Began) {
					lastPanPosition = touch.position;
					panFingerId = touch.fingerId;
				} else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved) {
					PanCamera(touch.position);
				}
			}

			break;

		case 2: // Zooming
			Vector2[] newPositions = new Vector2[]{Input.GetTouch(0).position, Input.GetTouch(1).position};
			if (!wasZoomingLastFrame) {
				lastZoomPositions = newPositions;
				wasZoomingLastFrame = true;
			} else {
				// Zoom based on the distance between the new positions compared to the 
				// distance between the previous positions.
				float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
				float oldDistance = Vector2.Distance(lastZoomPositions[0], lastZoomPositions[1]);
				float offset = newDistance - oldDistance;

				ZoomCamera(offset, ZoomSpeedTouch);

				lastZoomPositions = newPositions;
			}
			break;

		default: 
			wasZoomingLastFrame = false;
			break;
		}
	}

	void HandleMouse() {
		// On mouse down, capture it's position.
		// Otherwise, if the mouse is still down, pan the camera.

		Debug.Log ("Old Camera Mouse");

		if (enablePanning) {
			if (Input.GetMouseButtonDown(0)) {
				lastPanPosition = Input.mousePosition;
			} else if (Input.GetMouseButton(0)) {
				PanCamera(Input.mousePosition);
			}
		}

		// Check for scrolling to zoom the camera
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		ZoomCamera(scroll, ZoomSpeedMouse);
	}

	void PanCamera(Vector3 newPanPosition) {
		// Determine how much to move the camera
		Vector3 offset = mainCamera.ScreenToViewportPoint(lastPanPosition - newPanPosition);
		//Vector3 move = new Vector3(offset.x * PanSpeed, 0, offset.y * PanSpeed);

		Vector3 move = new Vector3(offset.x * PanSpeed, offset.y * PanSpeed, 0);

		// Perform the movement
		transform.Translate(move, Space.World);  

		// Ensure the camera remains within bounds.
		Vector3 pos = transform.position;
		pos.x = Mathf.Clamp(transform.position.x, BoundsX[0], BoundsX[1]);
		pos.z = Mathf.Clamp(transform.position.z, BoundsZ[0], BoundsZ[1]);
		transform.position = pos;

		// Cache the position
		lastPanPosition = newPanPosition;
	}

	void ZoomCamera(float offset, float speed) {
		if (offset == 0) {
			return;
		}

		mainCamera.fieldOfView = Mathf.Clamp(mainCamera.fieldOfView - (offset * speed), ZoomBounds[0], ZoomBounds[1]);
	}

	public void ZoomInPlayer()
	{
		Vector3 newCamPos = new Vector3 (player.position.x, player.position.y, mainCamera.transform.position.z);
		mainCamera.transform.position = newCamPos;
		mainCamera.fieldOfView = zoomingFoV;
		zoomInActive = true;
	}

	public void ZoomOutPlayer()
	{
		mainCamera.transform.position = startingCameraPosition;
		mainCamera.fieldOfView = startingFoV;
		zoomInActive = false;
		gridmodeActive = false;
	}

	public void GridModeCamera()
	{
		mainCamera.transform.position = gridmodeCameraPosition;
		mainCamera.fieldOfView = gridmodeFoV;
		gridmodeActive = true;
		zoomInActive = false;
	}

	public void SetStartingCamPos(Vector3 pos)
	{
		Vector3 newPos = new Vector3 (pos.x, pos.y + 2, startingCameraPosition.z);
		
		startingCameraPosition = newPos;
	}

	public void SetCurrentGrid(int direction)
	{
		switch (direction) {

		case 0:

			gridmodeCameraPosition.x -= 160f;
			transform.position = gridmodeCameraPosition;
			currentGrid--;
			break;

		case 1:

			gridmodeCameraPosition.x += 160f;
			transform.position = gridmodeCameraPosition;
			currentGrid++;
			break;
		}
	}

	public bool isZoomingIn()
	{
		return zoomInActive;	
	}

	public bool isGridmodeActive()
	{
		return gridmodeActive;
	}


	IEnumerator SingleOrDoubleTap(){
		yield return new WaitForSeconds(0.3f);
		if (touch.tapCount == 1) {
			Debug.Log ("Single");
		}
		else if(touch.tapCount == 2){

			//this coroutine has been called twice. 
			//We should stop the next one here otherwise we get two double tap

			StopCoroutine("singleOrDouble");
			Debug.Log ("Double");
		}
	}
}
