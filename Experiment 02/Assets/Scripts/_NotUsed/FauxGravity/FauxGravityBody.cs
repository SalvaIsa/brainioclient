﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FauxGravityBody : MonoBehaviour {

	public FauxGravityAttractor attractor;

	// Use this for initialization
	void Start () {

		Rigidbody myRigid = GetComponent<Rigidbody> ();

		myRigid.constraints = RigidbodyConstraints.FreezeRotation;
		myRigid.useGravity = false; 

	}
	
	// Update is called once per frame
	void Update () {
	
		attractor.attract (transform);
	}
}
