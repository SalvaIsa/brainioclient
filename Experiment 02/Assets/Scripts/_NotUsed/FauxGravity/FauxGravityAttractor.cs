﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FauxGravityAttractor : MonoBehaviour {

	public float gravity = -10f;

	public void attract(Transform body)
	{
		Vector3 gravityUp = (body.position - transform.position).normalized;
		Rigidbody bodyRigid = body.GetComponent<Rigidbody> ();

		bodyRigid.AddForce (gravityUp * gravity);

		Quaternion targetRotation = Quaternion.FromToRotation (body.up, gravityUp) * body.rotation;
		body.rotation = Quaternion.Slerp (body.rotation, targetRotation, 50 * Time.deltaTime);
	}

}
