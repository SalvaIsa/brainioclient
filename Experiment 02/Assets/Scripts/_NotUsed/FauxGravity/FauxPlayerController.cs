﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FauxPlayerController : MonoBehaviour {

	public float moveSpeed = 15;
	private Vector3 moveDir;
	private Rigidbody myRigidbody;

	void Start()
	{
		myRigidbody = GetComponent<Rigidbody> ();
	}

	void Update () 
	{
		float hor = Input.GetAxisRaw ("Horizontal");
		float ver = Input.GetAxisRaw ("Vertical");

		moveDir = new Vector3 (hor, 0.0f, ver).normalized;
	}

	void FixedUpdate()
	{
		myRigidbody.MovePosition (myRigidbody.position + transform.TransformDirection(moveDir) * moveSpeed * Time.deltaTime);
	}
}
