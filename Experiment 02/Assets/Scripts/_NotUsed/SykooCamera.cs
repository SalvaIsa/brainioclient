﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SykooCamera : MonoBehaviour {

	public Transform player;
	public float smooth;
	public float offsetZ;
	public float offsetY;

	private Vector3 velocity = Vector3.zero;

	void Start () {
		
	}
	
	void Update () {
	
		Vector3 pos = new Vector3 ();
		pos.x = player.position.x;
		pos.z = player.position.z - offsetZ;
		pos.y = player.position.y + offsetY;
		transform.position = Vector3.SmoothDamp (transform.position, pos, ref velocity, smooth * Time.deltaTime);
	

	}
}
