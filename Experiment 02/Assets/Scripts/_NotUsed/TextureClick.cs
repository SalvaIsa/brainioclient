﻿// Write black pixels onto the GameObject that is located
// by the script. The script is attached to the camera.
// Determine where the collider hits and modify the texture at that point.
//
// Note that the MeshCollider on the GameObject must have Convex turned off. This allows
// concave GameObjects to be included in collision in this example.
//
// Also to allow the texture to be updated by mouse button clicks it must have the Read/Write
// Enabled option set to true in its Advanced import settings.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProBuilder2.Common;

public class TextureClick : MonoBehaviour
{
	public LevelManager levelManager;
	//public MeshRenderer plane;
	public Camera cam;
	public bool selectionEnabled;

	public TexturePartition textPartition;

	private Color visorLabelColor = Color.yellow;
	private Color biasLabelColor = Color.red;
	private Color selectionLabelColor = Color.green;

	void Update()
	{
		if (!selectionEnabled)
			return;

		if (SpawningPlayer.IsPointerOverUIObject())
			return;
	
		if (!Input.GetMouseButton(0))
			return;

		RaycastHit visorPlaneHit = new RaycastHit();
		Color planeColor = new Color();

		RaycastHit[] hits;
		hits = Physics.RaycastAll(cam.ScreenPointToRay(Input.mousePosition), 100.0F);

//		DebuggingForGrowing_Zero (hits);

		for (int i = 0; i < hits.Length; i++) {
		
			RaycastHit hitto = hits[i];

			if (hitto.collider.tag == "PartitionedPlane") {

				AddPixelToList (hitto, biasLabelColor);
			}

//			if (hitto.collider.tag == "BiasPlane") {
//
//				AddPixelToList (hitto, biasLabelColor);
//
//			} else if (hitto.collider.tag == "SelectionPlane") {
//					
//	//			SelectBigPixel (hitto, selectionLabelColor);
//			}
//			else if (hitto.collider.tag != "Player") {
//					
//				AddPixelToList (hitto, visorLabelColor);
//			}
		
		}
			
		if (visorPlaneHit.transform == null)
			return;

	}
	
	void DebuggingForGrowing_Zero(RaycastHit[] hits)
	{
		float biasPixel = 0.0f;
		float visorPixel = 0.0f;

		int pixelX = -1;
		int pixelY = -1;

		Texture2D biasText = null;
		Texture2D visorText = null;

		for (int i = 0; i < hits.Length; i++) {

			RaycastHit hitto = hits [i];
			float pixValue = 0.0f;
			Texture2D tex = null;

			if (hitto.collider.tag == "BiasPlane" || hitto.collider.tag == "VisorPlane") {

				Renderer rend = hitto.transform.GetComponent<Renderer>();
				MeshCollider meshCollider = hitto.collider as MeshCollider;

				tex = rend.material.mainTexture as Texture2D;

				Vector2 pixelUV = hitto.textureCoord;

				pixelUV.x *= tex.width;
				pixelUV.y *= tex.height;

				pixelX = (int)pixelUV.x;
				pixelY = (int)pixelUV.y;

				pixValue = tex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).grayscale;
			}

			if (hitto.collider.tag == "BiasPlane") {	
				biasPixel = pixValue;
				biasText = tex;
			}
			if (hitto.collider.tag == "VisorPlane") {	
				visorPixel = pixValue;
				visorText = tex;
			}
		}

		if(!biasText.GetPixel(pixelX, pixelY).Equals(Color.yellow))
			DebuggingForGrowing_One (pixelX, pixelY, biasText, visorText);

	}

	void DebuggingForGrowing_One(int pixelX, int pixelY, Texture2D biasTxt, Texture2D visorTxt)
	{
		float biasValue = biasTxt.GetPixel (pixelX, pixelY).grayscale;
		float visorValue = visorTxt.GetPixel (pixelX, pixelY).grayscale;

//		biasTxt.SetPixel (pixelX, pixelY, Color.yellow);
//		biasTxt.Apply ();

		float deltaX = biasValue - visorValue;
		deltaX = Mathf.Round (deltaX * 1000f);// / 1000f;

		Debug.Log ("Delta X = " + deltaX);

		float right_biasValue = biasTxt.GetPixel (pixelX +1, pixelY).grayscale;
		float right_visorValue = visorTxt.GetPixel (pixelX +1, pixelY).grayscale;

		float right_deltaX = right_biasValue - right_visorValue;
		right_deltaX = Mathf.Round (right_deltaX * 1000f); // / 1000f;

		Debug.Log ("RIGHT Delta X = " + right_deltaX);

		Debug.Log ("Absalon " + Mathf.Abs (deltaX - right_deltaX));

//		if(Mathf.Abs(deltaX - right_deltaX) < 0.05)
//			DebuggingForGrowing_One (pixelX+1, pixelY, biasTxt, visorTxt);
		
	}

	void AddPixelToList(RaycastHit planeHit, Color newColor)
	{
		Renderer rend = planeHit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = planeHit.collider as MeshCollider;

		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
			return;

		Texture2D tex = rend.material.mainTexture as Texture2D;

		Vector2 pixelUV = planeHit.textureCoord;

		pixelUV.x *= tex.width;
		pixelUV.y *= tex.height;

		if (!tex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.black)
		   && !tex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.clear)) {

			var pixelX = (int)pixelUV.x;
			var pixelY = (int)pixelUV.y;

			tex.SetPixel(pixelX, pixelY, newColor);
			tex.Apply();

			Debug.Log ("Aggiungo " + pixelX + "-" + (int)(tex.height - pixelY));

			levelManager.AddToSelectedDamage (pixelX + "-" + (int)(tex.height - pixelY));
		}

	}

	private float damageThreshold;

	private void RecursiveColoring(int x, int y, Texture2D tex, Color newCol)
	{
		if (!tex.GetPixel (x, y).Equals (visorLabelColor)) {

			Color clickedPix = tex.GetPixel (x, y);
			Color leftPix = tex.GetPixel (x-1, y);
			Color rightPix = tex.GetPixel (x+1, y);
			Color upPix = tex.GetPixel (x, y+1);
			Color downPix = tex.GetPixel (x, y-1);

			tex.SetPixel (x, y, newCol);
			tex.Apply ();

//			LABColor clickedLAB = new LABColor (clickedPix);	Debug.Log ("LAB Pixel = " + clickedLAB);
//			LABColor leftLAB = new LABColor (leftPix);		Debug.Log ("LAB Left = " + leftLAB);

			pb_XYZ_Color xyz = pb_XYZ_Color.FromRGB(clickedPix);
			pb_CIE_Lab_Color lab = pb_CIE_Lab_Color.FromXYZ(xyz);

			pb_XYZ_Color xyz_left = pb_XYZ_Color.FromRGB(leftPix);
			pb_CIE_Lab_Color lab_left = pb_CIE_Lab_Color.FromXYZ(xyz_left);

			pb_XYZ_Color xyz_right = pb_XYZ_Color.FromRGB(rightPix);
			pb_CIE_Lab_Color lab_right = pb_CIE_Lab_Color.FromXYZ(xyz_right);

			pb_XYZ_Color xyz_up = pb_XYZ_Color.FromRGB(upPix);
			pb_CIE_Lab_Color lab_up = pb_CIE_Lab_Color.FromXYZ(xyz_up);

			pb_XYZ_Color xyz_down = pb_XYZ_Color.FromRGB(downPix);
			pb_CIE_Lab_Color lab_down = pb_CIE_Lab_Color.FromXYZ(xyz_down);

			Debug.Log("LEFT DE = " + pb_ColorUtil.DeltaE (lab, lab_left));
			Debug.Log("RIGHT DE = " + pb_ColorUtil.DeltaE (lab, lab_right));
			Debug.Log("UP DE = " + pb_ColorUtil.DeltaE (lab, lab_up));
			Debug.Log("DOWN DE = " + pb_ColorUtil.DeltaE (lab, lab_down));

			//Left
			if (pb_ColorUtil.DeltaE (lab, lab_left) < damageThreshold) {
						
				RecursiveColoring (x - 1, y, tex, newCol);
			}
//			//Right
//			if (pb_ColorUtil.DeltaE (lab, lab_right) < damageThreshold) {
//
//				RecursiveColoring (x + 1, y, tex, newCol);
//			}
			//Up
			if (pb_ColorUtil.DeltaE (lab, lab_up) < damageThreshold) {

				RecursiveColoring (x, y+1, tex, newCol);
			}
//			//Down
//			if (pb_ColorUtil.DeltaE (lab, lab_down) < damageThreshold) {
//
//				RecursiveColoring (x, y-1, tex, newCol);
//			}


//			//Left
//			if (Mathf.Abs (tex.GetPixel (x, y).grayscale - tex.GetPixel (x - 1, y).grayscale) < damageThreshold) {
//			
//				tex.SetPixel(x, y, newCol);
//				tex.Apply();
//				RecursiveColoring (x-1, y, tex, newCol);
//			}
//
//			//Right
//			if (Mathf.Abs (tex.GetPixel (x, y).grayscale - tex.GetPixel (x + 1, y).grayscale) < damageThreshold) {
//
//				tex.SetPixel(x, y, newCol);
//				tex.Apply();
//				RecursiveColoring (x+1, y, tex, newCol);
//			}
//
//
//			//Up
//			if (Mathf.Abs (tex.GetPixel (x, y).grayscale - tex.GetPixel (x, y + 1).grayscale) < damageThreshold){
//
//				tex.SetPixel(x, y, newCol);
//				tex.Apply();
//				RecursiveColoring (x, y +1, tex, newCol);
//			}
//
//			//Down
//			if (Mathf.Abs (tex.GetPixel (x, y).grayscale - tex.GetPixel (x, y - 1).grayscale) < damageThreshold) {
//			
//				tex.SetPixel(x, y, newCol);
//				tex.Apply();
//				RecursiveColoring (x, y -1 , tex, newCol);
//			}

		}
	}

	void SelectBigPixel(RaycastHit planeHit, Color newColor)
	{
		Renderer rend = planeHit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = planeHit.collider as MeshCollider;

		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
			return;

		Texture2D tex = rend.material.mainTexture as Texture2D;

		Vector2 pixelUV = planeHit.textureCoord;

		pixelUV.x *= tex.width;
		pixelUV.y *= tex.height;

		if (!tex.GetPixel ((int)pixelUV.x, (int)pixelUV.y).Equals (Color.black)) {

			var pixelX = (int)pixelUV.x;
			var pixelY = (int)pixelUV.y;

			tex.SetPixel (pixelX, pixelY, newColor);
			tex.SetPixel (pixelX+1, pixelY, newColor);
			tex.SetPixel (pixelX-1, pixelY, newColor);
			tex.SetPixel (pixelX, pixelY+1, newColor);
			tex.SetPixel (pixelX, pixelY-1, newColor);
		}

		tex.Apply();
	}
}


//		if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out visorPlaneHit))
//			return;
//
//	Vector3 fwd = transform.TransformDirection(Vector3.forward);
//	Debug.DrawRay(transform.position, fwd * 50, Color.green);
//	if (!Physics.Raycast (transform.position, fwd, out hit, 50))
//			return;

//SelectPixel (visorPlaneHit);


// Codice per switchare Touch - Mouse
//if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {
//
//	switch(Input.touchCount) {
//
//	case 1: 
//
//		Touch touch = Input.GetTouch (0);
//
//		if (touch.phase == TouchPhase.Began) 
//		{} 
//
//		break;
//
//	default: 
//		break;
//	}
//
//} else {}