﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour {

	public GameObject isometricCamera;
	public GameObject thirdPersTopDownCamera;

	private GameObject currentCamera;

	// Use this for initialization
	void Start () {

		currentCamera = isometricCamera;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.LeftControl)) {

			if (Input.GetKey(KeyCode.F1)) {
			
				Debug.Log ("Camera 1");

				currentCamera.SetActive (false);

				currentCamera = isometricCamera;

				currentCamera.SetActive (true);
			}

			if (Input.GetKey(KeyCode.F2)) {

				Debug.Log ("Camera 2");
				currentCamera.SetActive (false);

				currentCamera = thirdPersTopDownCamera;

				currentCamera.SetActive (true);
			}

			if (Input.GetKey(KeyCode.F3)) {

				Debug.Log ("Camera 3");
			}
		}
	}	
}
