﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkText : MonoBehaviour {

	public Text text;
	public RectTransform buttonsPanel;
	public Text userNameText;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("blinkText", 0f, 0.6f);
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonUp (0)) {

			buttonsPanel.gameObject.SetActive (true);
			userNameText.gameObject.SetActive (true);

			CancelInvoke ("blinkText");

			text.gameObject.SetActive (false);
		}
	}

	void blinkText()
	{
		if (text.IsActive ())
			text.gameObject.SetActive (false);
		else
			text.gameObject.SetActive (true);
	}
}
