﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SykooBullet : MonoBehaviour {

	public float speed;
	private float maxDistance = 0.0f;

	void Update () {
	
		transform.Translate (Vector3.forward * speed * Time.deltaTime);	
		maxDistance += 1 * Time.deltaTime;

		if (maxDistance >= 3)
			Destroy (this.gameObject);
	}
}
