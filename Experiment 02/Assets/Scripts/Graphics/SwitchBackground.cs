﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBackground : MonoBehaviour {

	[SerializeField] Texture2D baseTexture;
	[SerializeField] Texture2D overlaySette;
	[SerializeField] Texture2D allargataNormal;
	[SerializeField] Texture2D allargataSuper;

	public void ChangeTexture(int param)
	{
		switch (param) {

		case 0:
			GetComponent<MeshRenderer> ().material.mainTexture = baseTexture;
			break;
		case 1:
			GetComponent<MeshRenderer> ().material.mainTexture = overlaySette;
			break;
		case 2:
			GetComponent<MeshRenderer> ().material.mainTexture = allargataNormal;
			break;
		case 3:
			GetComponent<MeshRenderer> ().material.mainTexture = allargataSuper;
			break;
			
		default:
			break;
			

		}
	}
}
