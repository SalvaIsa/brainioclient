﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloodFillTexture : MonoBehaviour {

	enum Direction {Right, Left, Up, Down, 
		DiagUpRight, DiagUpLeft, DiagDownRight, DiagDownLeft, 
		QuadRightUp, QuadRightDown, QuadLeftUp, QuadLeftDown, QuadUpLeft, QuadUpRight, QuadDownLeft, QuadDownRight};

	private static Color nullColor = new Color(-1f, -1f, -1f);

	private int testX = 0;
	private int testY = 0;
	private	int round = 0;

	private bool coloring = false;

	private Texture2D currTex;
	private Color startSeedColor = nullColor;

	private Dictionary<Direction, bool> activeDirections;

	void Start(){
	
		activeDirections = new Dictionary<Direction, bool> ();

		activeDirections.Add (Direction.Right, true);
		activeDirections.Add (Direction.Left, true);
		activeDirections.Add (Direction.Up, true);
		activeDirections.Add (Direction.Down, true);

		activeDirections.Add (Direction.DiagUpRight, true);
		activeDirections.Add (Direction.DiagUpLeft, true);
		activeDirections.Add (Direction.DiagDownRight, true);
		activeDirections.Add (Direction.DiagDownLeft, true);
	
		activeDirections.Add (Direction.QuadRightUp, true);
		activeDirections.Add (Direction.QuadRightDown, true);
		activeDirections.Add (Direction.QuadLeftUp, true);
		activeDirections.Add (Direction.QuadLeftDown, true);
		activeDirections.Add (Direction.QuadUpLeft, true);
		activeDirections.Add (Direction.QuadUpRight, true);
		activeDirections.Add (Direction.QuadDownLeft, true);
		activeDirections.Add (Direction.QuadDownRight, true);
	}

	void Update () {

		if (coloring) {

			FloodFill ();
			return;
		}
	}

	public void StartFloodFill(Texture2D texture, int pixelX, int pixelY, Color startColor)
	{
		currTex = texture;
		testX = pixelX;
		testY = pixelY;
		startSeedColor = startColor;

		coloring = true;

	}

	public bool isColoring()
	{
		return coloring;
	}

	private void FloodFill()
	{
		bool found = false;

		//	UP
		if (activeDirections [Direction.Up])
			if (CheckPixelLimits (testX, testY + round)) 
			{
				if (startSeedColor.Equals (currTex.GetPixel (testX, testY + round))) {

					currTex.SetPixel (testX, testY + round, Color.yellow);
					found = true;
			} else if(round != 0)
					activeDirections [Direction.Up] = false;
			}

		// 	DOWN
		if (activeDirections [Direction.Down])
			if(CheckPixelLimits(testX, testY - round))
			{
				if (startSeedColor.Equals (currTex.GetPixel (testX, testY-round))) {

					currTex.SetPixel (testX, testY-round, Color.yellow);
					found = true;
			} else if(round != 0)
					activeDirections [Direction.Down] = false;
			}

		//	RIGHT
		if (activeDirections [Direction.Right]
		    && CheckPixelLimits (testX + round, testY)) {
		
				if (startSeedColor.Equals (currTex.GetPixel (testX+round, testY))) {
					currTex.SetPixel (testX+round, testY, Color.yellow);
					found = true;
				}  else if(round != 0)
					activeDirections [Direction.Right] = false;
			
			}

		//	LEFT
		if (activeDirections [Direction.Left]
			&& CheckPixelLimits (testX - round, testY)) {

			if (startSeedColor.Equals (currTex.GetPixel (testX - round, testY))) {
				currTex.SetPixel (testX - round, testY, Color.yellow);
				found = true;
			}  else if(round != 0)
				activeDirections [Direction.Left] = false;

		}

		//	DIAG UP RIGHT
		if (activeDirections [Direction.DiagUpRight]
			&& CheckPixelLimits (testX + round, testY + round)) {

			if (startSeedColor.Equals (currTex.GetPixel (testX + round, testY + round))) {
				currTex.SetPixel (testX + round, testY + round, Color.yellow);
				found = true;
			}  else if(round != 0)
				activeDirections [Direction.DiagUpRight] = false;

		}

		//	DIAG UP LEFT
		if (activeDirections [Direction.DiagUpLeft]
			&& CheckPixelLimits (testX - round, testY + round)) {

			if (startSeedColor.Equals (currTex.GetPixel (testX - round, testY + round))) {
				currTex.SetPixel (testX - round, testY + round, Color.yellow);
				found = true;
			}  else if(round != 0)
				activeDirections [Direction.DiagUpLeft] = false;

		}

		//	DIAG DOWN RIGHT
		if (activeDirections [Direction.DiagDownRight]
			&& CheckPixelLimits (testX + round, testY - round)) {

			if (startSeedColor.Equals (currTex.GetPixel (testX + round, testY - round))) {
				currTex.SetPixel (testX + round, testY - round, Color.yellow);
				found = true;
			}  else if(round != 0)
				activeDirections [Direction.DiagDownRight] = false;

		}

		//	DIAG DOWN LEFT
		if (activeDirections [Direction.DiagDownLeft]
			&& CheckPixelLimits (testX - round, testY - round)) {

			if (startSeedColor.Equals (currTex.GetPixel (testX - round, testY - round))) {
				currTex.SetPixel (testX - round, testY - round, Color.yellow);
				found = true;
			}  else if(round != 0)
				activeDirections [Direction.DiagDownLeft] = false;

		}

		bool[] activeQuads = new bool[]{ false, false, false, false, false, false, false, false };

		// *********	QUADS
		for (int qp = 1; qp < round; qp++) {

			//	QUAD RIGHT UP
			if(activeDirections [Direction.QuadRightUp]
				&& CheckPixelLimits(testX + round, testY + qp))
				if (startSeedColor.Equals (currTex.GetPixel (testX + round, testY + qp))) {
				
				if (Color.yellow.Equals (currTex.GetPixel (testX + round, testY + qp - 1))
					|| Color.yellow.Equals (currTex.GetPixel (testX + round - 1, testY + qp))) {
						currTex.SetPixel (testX + round, testY + qp, Color.yellow);
						found = true;

						activeQuads [0] = true;
					}
				}

//			//	QUAD RIGHT DOWN
//			if(CheckPixelLimits(testX + round, testY - qp))
//			if (startSeedColor.Equals (currTex.GetPixel (testX + round, testY - qp))) {
//				currTex.SetPixel (testX + round, testY - qp, Color.yellow);
//				found = true;
//
//				activeQuads [1] = true;
//			}

//			//	QUAD LEFT UP
//			if(CheckPixelLimits(testX - round, testY + qp))
//
//			if (startSeedColor.Equals (currTex.GetPixel (testX - round, testY + qp))) {
//				currTex.SetPixel (testX - round, testY + qp, Color.yellow);
//				found = true;
//			}
//
//			//	QUAD LEFT DOWN
//			if(CheckPixelLimits(testX - round, testY - qp))
//
//			if (startSeedColor.Equals (currTex.GetPixel (testX - round, testY - qp))) {
//				currTex.SetPixel (testX - round, testY - qp, Color.yellow);
//				found = true;
//			}
//
//			//	QUAD UP LEFT
//			if(CheckPixelLimits(testX - qp, testY + round))
//
//			if (startSeedColor.Equals (currTex.GetPixel (testX - qp, testY + round))) {
//				currTex.SetPixel (testX - qp, testY + round, Color.yellow);
//				found = true;
//			}
//
//			//	QUAD UP RIGHT
//			if(CheckPixelLimits(testX + qp, testY + round))
//			if (startSeedColor.Equals (currTex.GetPixel (testX + qp, testY + round))) {
//				currTex.SetPixel (testX + qp, testY + round, Color.yellow);
//				found = true;
//			}
//
//			// QUAD DOWN LEFT
//			if(CheckPixelLimits(testX - qp, testY - round))
//			if (startSeedColor.Equals (currTex.GetPixel (testX - qp, testY - round))) {
//				currTex.SetPixel (testX - qp, testY - round, Color.yellow);
//				found = true;
//			}
//
//			// QUAD DOWN RIGHT
//			if(CheckPixelLimits(testX + qp, testY - round))
//			if (startSeedColor.Equals (currTex.GetPixel (testX + qp, testY - round))) {
//				currTex.SetPixel (testX + qp, testY - round, Color.yellow);
//				found = true;
//			}

		}

		if (round > 1) {

			activeDirections [Direction.QuadRightUp] = activeQuads [0];
		}

		currTex.Apply ();

		if (!found)
			ResetVariables ();
		else
			round++;
	}

	public void ResetVariables()
	{
		Debug.Log ("Resetting..");

		coloring = false;

		testX = 0;
		testY = 0;
		round = 0;
		startSeedColor = nullColor;

		activeDirections [Direction.Up] = true;
		activeDirections [Direction.Down] = true;
		activeDirections [Direction.Left] = true;
		activeDirections [Direction.Right] = true;

		activeDirections [Direction.DiagUpRight] = true;
		activeDirections [Direction.DiagUpLeft] = true;
		activeDirections [Direction.DiagDownRight] = true;
		activeDirections [Direction.DiagDownLeft] = true;

		activeDirections [Direction.QuadRightUp] = true;
		activeDirections [Direction.QuadRightDown] = true;
		activeDirections [Direction.QuadLeftUp] = true;
		activeDirections [Direction.QuadLeftDown] = true;
		activeDirections [Direction.QuadUpLeft] = true;
		activeDirections [Direction.QuadUpRight] = true;
		activeDirections [Direction.QuadDownLeft] = true;
		activeDirections [Direction.QuadDownRight] = true;

	}

	private bool CheckPixelLimits(int x, int y)
	{
		if (x < 0 || y < 0 || x > currTex.width || y > currTex.height) {
			
			return false;
		}
		
		return true;
	}
}
