﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyTextureExtension {

	public struct Point
	{
		public short x;
		public short y;
		public Point(short aX, short aY) { x = aX; y = aY; }
		public Point(int aX, int aY) : this((short)aX, (short)aY) { }
	}

	public static void FloodFillPreview(this Texture2D aTex, int aX, int aY, Texture2D biasTex, Texture2D visorTex)
	{
		Color aFillColor = Color.yellow;

		int w = aTex.width;
		int h = aTex.height;

		Color[] colors = aTex.GetPixels();
		Color[] biasColors = biasTex.GetPixels();
		Color[] visorColors = visorTex.GetPixels();

		Color refCol = colors[aX + aY * w];
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();

			for (int i = current.x; i < w; i++)
			{
				Color C = colors[i + current.y * w];
				Color BiasCol = biasColors[i + current.y * w];
				if (C != refCol || C == aFillColor || BiasCol.a == 0.0f)
					break;

				colors[i + current.y * w] = aFillColor;
				biasColors [i + current.y * w] = aFillColor;
				visorColors [i + current.y * w] = aFillColor;

				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				Color C = colors[i + current.y * w];
				Color BiasCol = biasColors[i + current.y * w];
				if (C != refCol || C == aFillColor || BiasCol.a == 0.0f)
					break;

				colors[i + current.y * w] = aFillColor;
				biasColors [i + current.y * w] = aFillColor;
				visorColors [i + current.y * w] = aFillColor;

				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}

		biasTex.SetPixels (biasColors);
		visorTex.SetPixels (visorColors);
	}

	public static void FloodFillPermanent(this Texture2D aTex, int aX, int aY, Texture2D biasTex, Texture2D visorTex, 
		Texture2D clusterTex, LevelManager manager)
	{
		Color aFillColor = GetDrawingColor ();

		int w = aTex.width;
		int h = aTex.height;

		Color[] colors = aTex.GetPixels();
		Color[] biasColors = biasTex.GetPixels();
		Color[] visorColors = visorTex.GetPixels();
		Color[] clusteredColors = clusterTex.GetPixels();

		Color refCol = colors[aX + aY * w];
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();

			for (int i = current.x; i < w; i++)
			{
				Color C = colors[i + current.y * w];
				Color BiasCol = biasColors[i + current.y * w];
				if (C != refCol || C == aFillColor || BiasCol.a == 0.0f)
					break;
				
				manager.AddToSelectedDamage (i + "-" + (int)(visorTex.height - current.y));
				manager.IncreasePixelCluster (clusteredColors[i + current.y * w]);

				colors[i + current.y * w] = aFillColor;
				biasColors [i + current.y * w] = aFillColor;
				visorColors [i + current.y * w] = aFillColor;

				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				Color C = colors[i + current.y * w];
				Color BiasCol = biasColors[i + current.y * w];
				if (C != refCol || C == aFillColor || BiasCol.a == 0.0f)
					break;
				
				manager.AddToSelectedDamage (i + "-" + (int)(visorTex.height - current.y));
				manager.IncreasePixelCluster (clusteredColors[i + current.y * w]);

				colors[i + current.y * w] = aFillColor;
				biasColors [i + current.y * w] = aFillColor;
				visorColors [i + current.y * w] = aFillColor;

				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}

		biasTex.SetPixels (biasColors);
		visorTex.SetPixels (visorColors);
	}

	private static Color GetDrawingColor()
	{
		int rand = Random.Range (0, 3);
		Color c = new Color();

		switch(rand)
		{
		case 0:	// 00BCD4
			ColorUtility.TryParseHtmlString ("#66ff33", out c);		//	c = new Color (0f, 0.411f, 0.36f);
			break;
		case 1:	// NO 0288D1
			ColorUtility.TryParseHtmlString ("#40ff00", out c);		//	c = new Color (0.156f, 0.171f, 0.576f);
			break;
		case 2:	// NO 1565C0
			ColorUtility.TryParseHtmlString ("#66ff00", out c);		//	c = new Color (0.082f, 0.396f, 0.752f);
			break;
		case 3: // 	NO 3949AB
			ColorUtility.TryParseHtmlString ("#66ff00", out c);  	//	c = new Color (0.011f, 0.607f, 0.898f);
			break;

		default:
			c = Color.green;
			break;
		}

		return c;
	}
}
