﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexturePartition : MonoBehaviour {

	public LevelManager levelManager;
	public MeshRenderer visorSourcePlane;
	public MeshRenderer biasSourcePlane;
	public MeshRenderer altroPlane;

	public bool afterwork;
	public bool working;

	private LevelBuilder levelBuilder;
	private PartitionedTextureClicking textClicking;

	private Texture2D m_BiasSourceTexture;
	private Texture2D m_VisorSourceTexture;
	private Texture2D m_ClusteredSourceTexture;
	private Texture2D m_SuperpixelSourceTexture;

	private Texture2D backup_BiasTexture;
	private Texture2D backup_VisorTexture;

	private Texture2D m_DestiVisorTexture;
	private Texture2D m_DestiClusterTexture;

	private int xStart = -1;
	private int yStart = -1;

	private int textWidth; 
	private int textHeight;

	private MyHistogram myHisto;

	void Awake()
	{
		levelBuilder = levelManager.GetComponent<LevelBuilder> ();

		m_BiasSourceTexture = biasSourcePlane.material.mainTexture as Texture2D;
		m_VisorSourceTexture = visorSourcePlane.material.mainTexture as Texture2D;
		m_ClusteredSourceTexture = transform.parent.GetComponent<LayerParameters> ().GetClusteredTexture ();
		m_SuperpixelSourceTexture = transform.parent.GetComponent<LayerParameters> ().GetSuperPixelTexture ();

		backup_BiasTexture = m_BiasSourceTexture;
		backup_VisorTexture = m_VisorSourceTexture;

		textClicking = altroPlane.GetComponent<PartitionedTextureClicking> ();
		myHisto = altroPlane.GetComponent<MyHistogram> ();
	}

	void Update()
	{
		if (working) {

			Partitioning ();

			working = false;
		} 

		if (afterwork) {
		
			Transferring ();

			afterwork = false;
		}
	}

	private void Partitioning()
	{
		Color[] sourcePixels = m_VisorSourceTexture.GetPixels(0);
		Color[] destinationPixels = m_DestiVisorTexture.GetPixels(0);

		Color[] sourceSegmentedPixels = m_ClusteredSourceTexture.GetPixels(0);
		Color[] destiSegmentedPixels = m_DestiClusterTexture.GetPixels(0);

		int dPix = 0;

		for (int y = yStart; y < yStart + textHeight; ++y)
		{
			for (int x = xStart; x < xStart + textWidth; ++x)
			{
				int index = (y * m_VisorSourceTexture.width + x);

				destinationPixels[dPix] = sourcePixels[index];
				destiSegmentedPixels[dPix] = sourceSegmentedPixels[index];

				dPix++;
			}
		}

		m_DestiClusterTexture.SetPixels(destiSegmentedPixels, 0);
		m_DestiClusterTexture.Apply();

		m_DestiVisorTexture.SetPixels(destinationPixels, 0);
		m_DestiVisorTexture.Apply();

		altroPlane.material.EnableKeyword ("_DETAIL_MULX2");
		altroPlane.material.SetTexture("_MainTex", m_DestiVisorTexture);
		altroPlane.material.SetTexture("_DetailAlbedoMap", m_DestiVisorTexture);

		altroPlane.transform.localScale = new Vector3 (textWidth, 1.0f, textHeight);
		altroPlane.gameObject.SetActive (true);

//		myHisto.SetParameters ();
		myHisto.ProvaDue_SetParameters ();
	}

	private void Transferring()
	{
		Color[] visorPixels = m_VisorSourceTexture.GetPixels(0);
		Color[] biasPixels = m_BiasSourceTexture.GetPixels(0);
		Color[] destinationPixels = m_DestiVisorTexture.GetPixels(0);

		int dPix = 0;
		for (int y = yStart; y < yStart + textHeight; ++y)
		{
			for (int x = xStart; x < xStart + textWidth; ++x)
			{
				int index = (y * m_VisorSourceTexture.width + x);

				/*
					Senza questo controllo, la porzione nella BiasLabel assume i colori scuri
					del VisorLabel, è un effetto anche carino ma valutabile.

-					Inoltre, qua posso scegliere di che colore fare i pixel su entrambi i visori, 
					se li voglio diversificare.
				*/
				if(destinationPixels[dPix].Equals(Color.green))
				{
					//	Debug.Log ("Aggiungo " + x + "-" + (int)(m_VisorSourceTexture.height - y));
					levelManager.AddToSelectedDamage (x + "-" + (int)(m_VisorSourceTexture.height - y));

					visorPixels[index] = destinationPixels[dPix];
					biasPixels[index] = destinationPixels[dPix];
				}
			
				dPix++;
			}
		}

		m_VisorSourceTexture.SetPixels(visorPixels, 0);
		m_VisorSourceTexture.Apply();

		m_BiasSourceTexture.SetPixels(biasPixels, 0);
		m_BiasSourceTexture.Apply();

		textClicking.selectionEnabled = false;
		//altroPlane.gameObject.SetActive (false);

	}

	bool switched = false;
	public void SwitchPartitionedTexture()
	{
		if (!switched) {
		
			altroPlane.material.SetTexture ("_MainTex", m_DestiClusterTexture);
			altroPlane.material.SetTexture ("_DetailAlbedoMap", m_DestiClusterTexture);
		
			switched = true;
		} else {
		
			altroPlane.material.SetTexture("_MainTex", m_DestiVisorTexture);
			altroPlane.material.SetTexture("_DetailAlbedoMap", m_DestiVisorTexture);

			switched = false;
		}
	}

	public void SwitchFullTexture(int param)
	{
		if (!switched) {

			if(param == 0)	//CLUSTERED
				biasSourcePlane.material.SetTexture("_MainTex", m_ClusteredSourceTexture);
			else if(param == 1)
				biasSourcePlane.material.SetTexture("_MainTex", m_SuperpixelSourceTexture);
			
			switched = true;
		
		} else {

			biasSourcePlane.material.SetTexture("_MainTex", m_BiasSourceTexture);
			switched = false;
		}
	}

	public void ResetBackupsTexture()
	{
		m_BiasSourceTexture.SetPixels (backup_BiasTexture.GetPixels());
		m_VisorSourceTexture.SetPixels (backup_VisorTexture.GetPixels());

	}

	public void ApplyTextureChanges()
	{
		m_BiasSourceTexture.Apply ();
		m_VisorSourceTexture.Apply ();
	}

	public void UpdateBackupTextures()
	{
		backup_BiasTexture = levelBuilder.GetReadableTexture (m_BiasSourceTexture);
		backup_VisorTexture = levelBuilder.GetReadableTexture (m_VisorSourceTexture);
	}

	public void CountClustersInTexture(int currSlice)
	{
		ApplicationModel.sliceClusters[currSlice] = new Dictionary<Color, int> ();

		Color[] clusPix = m_ClusteredSourceTexture.GetPixels ();
		
		foreach (Color col in clusPix) {
		
			if (!ApplicationModel.sliceClusters[currSlice].ContainsKey (col))
				ApplicationModel.sliceClusters[currSlice].Add (new Color (col.r, col.g, col.b), 0);
		}
	}

	public void SetSourcePlanes(MeshRenderer visorSource, MeshRenderer biasSource)
	{
		m_BiasSourceTexture = biasSource.material.mainTexture as Texture2D;
		m_VisorSourceTexture = visorSource.material.mainTexture as Texture2D;

		backup_BiasTexture = levelBuilder.GetReadableTexture (m_BiasSourceTexture);
		backup_VisorTexture = levelBuilder.GetReadableTexture (m_VisorSourceTexture);

		m_ClusteredSourceTexture = transform.parent.GetComponent<LayerParameters> ().GetClusteredTexture ();
		m_SuperpixelSourceTexture = transform.parent.GetComponent<LayerParameters> ().GetSuperPixelTexture ();
	}
		
	public void SetPartitionParameter(SelectionBox box)
	{
		if (box.DtapTextureX >= box.AtapTextureX && box.DtapTextureY <= box.AtapTextureY) {

			xStart = box.AtapTextureX;
			yStart = box.DtapTextureY;

		} else if (box.DtapTextureX >= box.AtapTextureX && box.DtapTextureY >= box.AtapTextureY) {
			
			xStart = box.AtapTextureX;
			yStart = box.AtapTextureY;

		} else if (box.DtapTextureX <= box.AtapTextureX && box.DtapTextureY <= box.AtapTextureY) {
			
			xStart = box.DtapTextureX;
			yStart = box.DtapTextureY;

		} else if (box.DtapTextureX <= box.AtapTextureX && box.DtapTextureY >= box.AtapTextureY) {
			
			xStart = box.DtapTextureX;
			yStart = box.AtapTextureY;

		}

		textWidth = Mathf.Abs (box.AtapTextureX - box.DtapTextureX);
		textHeight = Mathf.Abs (box.AtapTextureY - box.DtapTextureY);
	
		m_DestiVisorTexture = new Texture2D(textWidth, textHeight, TextureFormat.RGBA32, false);
		m_DestiClusterTexture = new Texture2D(textWidth, textHeight, TextureFormat.RGBA32, false);

		working = true;
	}

	public Color GetPixelCluster(int pixelX, int pixelY)
	{
		return m_ClusteredSourceTexture.GetPixel (pixelX, pixelY);
	}

	public Color GetSuperPixel(int pixelX, int pixelY)
	{
		return m_SuperpixelSourceTexture.GetPixel (pixelX, pixelY);
	}

	public Texture2D GetClusteredTexture()
	{
		return m_ClusteredSourceTexture;
	}

	public Texture2D GetSuperpixelTexture()
	{
		return m_SuperpixelSourceTexture;
	}

	public Texture2D GetVisorTexture()
	{
		return m_VisorSourceTexture;
	}
}
