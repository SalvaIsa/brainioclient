﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureRecoloring : MonoBehaviour {

	public Color backColor;
	public float brainR;
	public float brainG;
	public float brainB;

	public bool countingDone;
	public bool coloring;

	public IntTimer gameTimer;

	//*********

	int testX = 0;
	int testY = 0;
	List<Vector2> seeds = new List<Vector2>();
	List<Vector2> toColor = new List<Vector2>();
	Color startSeedColor;
	//*********

	private TexturePartition textPartition;
	private MeshRenderer biasRenderer;
	private MeshRenderer visorRenderer;
	private int currentIndex;
	private float threshold;

	private int nonWhitePixels;

	private bool zawaDone;
	private bool normalDone;
	private bool txtPartSetupped;

	public static readonly Color ZaWarudoColor = new Color (0.988f, 0.62f, 0.259f, 0.0f);
	public static readonly Color NormalColor = new Color (0.545f, 0.765f, 0.29f, 0.0f); //new Color (0.95f, 0.95f, 0.95f, 0.0f); 

	void Start () {

		textPartition = GetComponent<TexturePartition> ();
		biasRenderer = GetComponent<MeshRenderer> ();

		zawaDone = false;
		normalDone = false;
		countingDone = false;
		txtPartSetupped = false;
	}

	void Update () 
	{
		if (coloring) {

			if (!countingDone) {
			
				CountNonWhitePixels ();
				countingDone = true;
			}

			ChangeBackground ();

			if (backColor.Equals (NormalColor)) {
			
				normalDone = true;
					
				if (textPartition != null && visorRenderer != null && !txtPartSetupped) {

					textPartition.SetSourcePlanes (visorRenderer, biasRenderer);
					textPartition.CountClustersInTexture (currentIndex-1);

					txtPartSetupped = true;
				}
			}
			else if (backColor.Equals(ZaWarudoColor))
				zawaDone = true;
			
			coloring = false;
		}
			
	}

	void AddingStartSeed()
	{
		if (!Input.GetMouseButton(0))
			return;

		RaycastHit visorPlaneHit = new RaycastHit();
		Color planeColor = new Color();

		RaycastHit[] hits;
		hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition), 100.0F);

		for (int i = 0; i < hits.Length; i++) 
		{
			RaycastHit hitto = hits[i];

			if (hitto.collider.tag == "BiasPlane") {

				Renderer rend = hitto.transform.GetComponent<Renderer>();
				MeshCollider meshCollider = hitto.collider as MeshCollider;

				if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
					return;

				Texture2D tex = rend.material.mainTexture as Texture2D;

				Vector2 pixelUV = hitto.textureCoord;

				pixelUV.x *= tex.width;
				pixelUV.y *= tex.height;

				var pixelX = (int)pixelUV.x;
				var pixelY = (int)pixelUV.y;			

//				testX = pixelX;
//				testY = pixelY;
//				seeds.Add (new Vector2 (pixelX, pixelY));

				tex.FloodFillArea(pixelX,pixelY,Color.white);
				tex.Apply ();

				startSeedColor = tex.GetPixel (pixelX, pixelY);
				Debug.Log ("Seed added, color = " + startSeedColor);

				//newTestong = true;
			}
		}

	}

	public void CountNonWhitePixels()
	{
		Texture2D tex = biasRenderer.material.mainTexture as Texture2D;

		Color[] colors = tex.GetPixels ();

		int nonWhite = 0;
		for(int x = 0; x < colors.Length; x++)
		{
			if (!(colors [x].a == 0))
			{
				nonWhite++;
			}
		}
			
		nonWhitePixels = nonWhite;

		gameTimer.SetMaxTimeAt (currentIndex, nonWhitePixels);

		if (currentIndex == ApplicationModel.biasTextures.Length)
			gameTimer.EnableStart ();
	}

	public void SetVisorRenderer(MeshRenderer visorSource)
	{
		visorRenderer = visorSource;
	}

	public void SetCurrentIndex(int i)
	{
		currentIndex = i;
	}

	void ChangeBackground()
	{
		Texture2D tex = biasRenderer.material.mainTexture as Texture2D;

		Color[] colors = tex.GetPixels ();

		for(int x = 0; x < colors.Length; x++)
		{
			if (colors [x].a == 0)// || colors [x].Equals(ZaWarudoColor) || colors [x].Equals(NormalColor))
				colors [x] = backColor;
			else {

				colors [x].r *= brainR;
				colors [x].g *= brainG;
				colors [x].b *= brainB;
			}
		}
			
		tex.SetPixels (colors);
		tex.Apply ();
	}
		
	public void GetRandomBackGround()
	{
		int r = Random.Range (0, 3);

		switch (r) {

		case 0:
			backColor = new Color (1f,1f,1f); //Color (0.99f, 0.62f, 0.26f);
			brainB = 2f;
			break;
		case 1:
			backColor = new Color (1f,1f,1f); //new Color (0.48f, 0.75f, 0.99f);
			brainR = 2f;
			break;
		case 2:
			backColor = new Color (1f,1f,1f); //new Color (0.51f, 0.99f, 0.5f);
			brainR = 1.3f;
			brainB = 1.3f;
			break;
		
		}

		coloring = true;
	}

	public void SetZawaBackground()
	{
		if (!zawaDone) {

			normalDone = false;

			backColor = ZaWarudoColor;

			brainR = 1.0f;
			brainG = 1.0f;
			brainB = 1.0f;

			coloring = true;
		}
	}

	public void SetNormalBackground()
	{
		if (!normalDone) {

			zawaDone = false;

			backColor = NormalColor;

			brainR = 1.0f;
			brainG = 1.0f;
			brainB = 1.0f;

			coloring = true;
		}
	}

	public bool isNormalDone()
	{
		return normalDone;
	}

	public int GetNonWhitePixels()
	{
		return nonWhitePixels;
	}
}
