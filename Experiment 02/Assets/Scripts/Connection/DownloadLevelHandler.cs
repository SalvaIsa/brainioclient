﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DownloadLevelHandler : MonoBehaviour {

	public RectTransform loadingPanel;

	private Text loadingText;
	private string test_url;
	private WWW www_post;

	public bool debugMode;
	public TextAsset debugLevel;

	void Start () {

		loadingText = loadingPanel.GetComponentInChildren<Text> ();
	}

	public void DownloadLevel(int id)
	{
		loadingPanel.gameObject.SetActive (true);

		if(debugMode)
			loadingText.text = "Inizializzando url e WWW";

		//test_url = "http://localhost:8080/MindCraft/LevelsController/GetSlices";
		test_url = "http://localhost:8080/MindCraft/LevelsController/GetSlicesZip";

		WWWForm form = new WWWForm();
		form.AddField("level", id);
		form.AddField("user", ApplicationModel.userName);

		www_post = new WWW(test_url, form);

		//StartCoroutine(WaitForRequest(www_post, id));
		StartCoroutine(WaitForZip(www_post, id));

		if(debugMode)
			loadingText.text = "Scaricando Livello " + id;

	}

	public void DownloadDebugLevel()
	{
		List<ImageMatrix> result = ParseMultipleMatrices (debugLevel.text);

		ApplicationModel.imageMatrices = result;
		ApplicationModel.level = 1;

		SceneManager.LoadScene("GameScene", LoadSceneMode.Single);

	}

	IEnumerator WaitForZip(WWW www, int id)
	{
		string zipPath = Application.temporaryCachePath + "/tempZip_" + id + ".zip";
		string exportPath = Application.temporaryCachePath + "/unzip_" + id;
		string imagePath = exportPath + "/s" + id + "_bias ";

		Debug.Log (exportPath);

		while (!www.isDone) {
			Debug.Log("downloaded " + www.progress + " %...");
			yield return null;
		}

		var data = www.bytes;
		File.WriteAllBytes(zipPath, data);
		ZipUtil.Unzip(zipPath, exportPath);

		List<string> fileNames = new List<string>( Directory.GetFiles(exportPath) );  

		/*
			Fare robe col file
		*/

		ApplicationModel.biasTextures = new Object[fileNames.Count];

		for (int x = 0; x < fileNames.Count; x++) 
		{
			string imPath = imagePath + "(" + (x+1) + ").png";

			Debug.Log (imagePath);

			//Texture2D tex =  new Texture2D(1, 1);
			var imageData = File.ReadAllBytes(imPath);

			ApplicationModel.biasTextures [x] = new Texture2D (1, 1);
			(ApplicationModel.biasTextures[x] as Texture2D).LoadImage(imageData);
		}
		//*************************

		File.Delete(zipPath);

		ApplicationModel.slicePath = imagePath;
		ApplicationModel.slicesCount = fileNames.Count;
		ApplicationModel.level = id;
		SceneManager.LoadScene("GameScene", LoadSceneMode.Single);

	}

	IEnumerator WaitForRequest(WWW www, int id)
	{
		yield return www;

		if (www.error == null)
		{
			if(debugMode)
				loadingText.text = "Parserizzando Matrice";

			List<ImageMatrix> result = ParseMultipleMatrices (www.text);

			if(debugMode)
				loadingText.text = "Numero di matrici: " + result.Count;

			ApplicationModel.imageMatrices = result;
			ApplicationModel.level = id;

			SceneManager.LoadScene("GameScene", LoadSceneMode.Single);

		} else {

			if(debugMode)
				loadingText.text = "WWW Error: "+ www.error;
		}    
	}

	private List<ImageMatrix> ParseMultipleMatrices(string text)
	{
		List<ImageMatrix> imageMatrices = new List<ImageMatrix>();

		string input = text;
		string[] matrices = input.Split (new[] { "****" }, System.StringSplitOptions.RemoveEmptyEntries);

		Debug.Log ("Ho trovato matrici num " + matrices.Length);

		for (int m = 0; m < matrices.Length; m++) {

			ImageMatrix im = new ImageMatrix();

			string[] lines = matrices[m].Split (new[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
			float[,] spaces = new float[lines.Length, 176];	// numero di colonne

			for (int i = 0; i < lines.Length; i++) {
				string st = lines[i];
				string[] nums = st.Split(new[] { ',' });

				for (int j = 0; j < nums.Length; j++) 
				{
					float val;
					if (float.TryParse (nums[j], out val))
						spaces[i,j] = val;
					else
						spaces[i,j] = -1f;

				}
			}

			im.values = spaces;
				
			imageMatrices.Add (im);
		}

		return imageMatrices;
	}
}
