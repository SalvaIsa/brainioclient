﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class MatchMakingConnection : MonoBehaviour {

	[SerializeField] SceneManagement sceneManager;
	[SerializeField] Text loadingText;

	private string currentUrl;
	private WWW www_post;

	//	LIVE 	players searching
	private bool liveFinding;
	private int serverThreshold;
	private float timer;

	private bool amIcreator;
	private string creatorUsername;

	//	OFFLINE FAKE 	players searching
	private bool searching;
	private bool completed;
	private bool backCompleted;
	private bool myTeamDone;
	private bool oppoTeamDone;

	private int thresholdPlayer;
	private int myTeamCount;
	private int oppoTeamCount;

	private string[] namesList;

	void Start () {

		liveFinding = false;
		amIcreator = false;
		serverThreshold = 2;
		timer = serverThreshold;

		thresholdPlayer = Random.Range (1, 2);

		myTeamCount = 1;

		namesList = new string[4];
		namesList [0] = "Player_42";
		namesList [1] = "Player_122";
		namesList [2] = "Player_313";
		namesList [3] = "Player_777";
	}
	
	// Update is called once per frame
	void Update () {

		if (!ApplicationModel.serverOffline) {

			if (completed) {
				
				searching = false;
				completed = false;

				string[] mates = sceneManager.GetTeamMates ();

				Debug.Log(mates[0]);
				Debug.Log(mates[1]);

				ApplicationModel.firstTeamMate = mates [0];
				ApplicationModel.secondTeamMate = mates [1];

				if (amIcreator) {
					
					loadingText.text = "Creating Match..";
					CreateMatch ();
				
				} else {
				
					loadingText.text = "Waiting Match..";
					GetMatch ();
				}
			}
				
			if (searching) {
				
				if (!liveFinding)
					timer += Time.deltaTime;

				if ((int)timer >= serverThreshold) {
			
					Debug.Log ("Tic Tac");

					FindLiveMatch ();

					liveFinding = true;
					timer = 0.0f;

				}
			}
		} else {

			if (myTeamDone && oppoTeamDone)
				completed = true;

			if (completed) {
			
				backCompleted = true;
			
				searching = false;
				completed = false;
			
				myTeamDone = false;
				oppoTeamDone = false;
			
				loadingText.text = "Creating Match..";
			
				CreateMatch ();
			
			}

			if (searching) {
			
				timer += Time.deltaTime;

				if ((int)timer >= thresholdPlayer) {

					int friendOrFoe;

					if (myTeamDone)
						friendOrFoe = 0;
					else if (oppoTeamDone)
						friendOrFoe = 1;
					else
						friendOrFoe = Random.Range (0, 2);

					string playName = namesList [Random.Range (0, namesList.Length)];

					if (friendOrFoe == 1) {

						sceneManager.AddPlayer (playName, true);
						myTeamCount++;
				
						if (myTeamCount == 3)
							myTeamDone = true;

					} else {
					
						sceneManager.AddPlayer (playName, false);
						oppoTeamCount++;

						if (oppoTeamCount == 3)
							oppoTeamDone = true;
				
					}
				
					timer = 0.0f;
					thresholdPlayer = Random.Range (1, 2);
				}

			} else {
			
				timer = 0.0f;
		
			}
		}
	}
		
	public void EnableSearching(bool val)
	{
		searching = val;
	}

	public bool IsCompleted()
	{
		return backCompleted;
	}

	public void ResetFields()
	{
		loadingText.text = "Waiting for players..";

		backCompleted = false;

		completed = false;
		searching = false;
		myTeamDone = false;
		oppoTeamDone = false;

		myTeamCount = 1;
		oppoTeamCount = 0;
	}

	private void FindLiveMatch()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/UserController/FindLiveMatch";

		WWWForm form = new WWWForm();
		form.AddField("userName", ApplicationModel.userName);

		www_post = new WWW (currentUrl, form);

		StartCoroutine(WaitForFindingLiveMatch(www_post));

	}

	IEnumerator WaitForFindingLiveMatch(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode matchNode = JSON.Parse(www.text);

			int liveId = matchNode ["id"].AsInt;
			string creatorName = matchNode ["creatorUsername"];
			string firstTeammate = matchNode ["firstMateUsername"];
			string secondTeammate = matchNode ["secondMateUsername"];
			string firstOpponent = matchNode ["firstOppUsername"];
			string secondOpponent = matchNode ["secondOppUsername"];
			string thirdOpponent = matchNode ["thirdOppUsername"];

			ApplicationModel.waitingId = liveId;
			creatorUsername = creatorName;
			if (creatorName.Equals (ApplicationModel.userName)) {

				ApplicationModel.amIcreator = true;
				amIcreator = true;
			}
			
			sceneManager.AddPlayerDirectly (creatorName, true, 0);

			if (firstTeammate != null)
				sceneManager.AddPlayerDirectly (firstTeammate, true, 1);

			if (secondTeammate != null)
				sceneManager.AddPlayerDirectly (secondTeammate, true, 2);

			if (firstOpponent != null)
				sceneManager.AddPlayerDirectly (firstOpponent, false, 0);

			if (secondOpponent != null)
				sceneManager.AddPlayerDirectly (secondOpponent, false, 1);

			if (thirdOpponent != null) {

				sceneManager.AddPlayerDirectly (thirdOpponent, false, 2);
				completed = true;
			}
			
			liveFinding = false;

		} else {
			
			Debug.Log (www.error);
		}
	}

	private void CreateMatch()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/UserController/CreateMatch";

		WWWForm form = new WWWForm();
		form.AddField("username", ApplicationModel.userName);
		form.AddField("waitingId", ApplicationModel.waitingId);

		www_post = new WWW (currentUrl, form);

		StartCoroutine(WaitForCreatingMatch(www_post));
	}

	IEnumerator WaitForCreatingMatch(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode matchNode = JSON.Parse(www.text);

			ApplicationModel.matchId = matchNode ["id"].AsInt;

			sceneManager.GoToGameScene ();

		} else {

			/*
				TODO Al momento la mancanza di connessione indica che siamo da Telefono, però deve uscire un avviso 
				perchè senza connessione non si può giocare.
			*/
			Debug.Log (www.error);

			sceneManager.GoToGameScene ();
		}
	}

	private void GetMatch()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/UserController/GetMatch";

		WWWForm form = new WWWForm();
		form.AddField("creatorUsername", creatorUsername);

		www_post = new WWW (currentUrl, form);

		StartCoroutine(WaitForGettingMatch(www_post));
	}

	IEnumerator WaitForGettingMatch(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode matchNode = JSON.Parse(www.text);

			int mId = matchNode ["id"].AsInt;

			if (mId == -1) {
			
				Debug.Log ("No Match yet..");

				GetMatch ();

			} else {

				Debug.Log ("Match Found! " + mId);

				ApplicationModel.matchId = mId;
				sceneManager.GoToGameScene ();
			
			}
		} else {
			
			Debug.Log (www.error);
		}
	}
}
