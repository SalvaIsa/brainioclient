﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GetLevelsConnect : MonoBehaviour {

	public GameObject prefabButton;
	public RectTransform parentPanel;
	public Image loadingCircle;
	public InputField userNick;

	private string test_url;
	private WWW www_get;

	public bool debugMode;

	void Start () {
		
		test_url = "http://localhost:8080/MindCraft/LevelsController/GetLevels";

		if (debugMode) {
			www_get = new WWW (test_url);
			StartCoroutine(WaitForRequest(www_get));
		}
	}

	public void GetLevels()
	{
		www_get = new WWW (test_url);
		StartCoroutine(WaitForRequest(www_get));	
	}

	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;

		if (www.error == null)
		{
			Debug.Log("WWW Text: "+ www.text);
			ParseLevelsButtons (www.text);
					
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	}

	private void ParseLevelsButtons(string text)
	{
		string[] levels = text.Split (new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);

		levels [0] = levels [0].Replace ("[","");
		levels [levels.Length-1] = levels [levels.Length-1].Replace ("]","");

		loadingCircle.gameObject.SetActive(false);

		foreach (string c in levels) {
		
			GameObject goButton = (GameObject)Instantiate(prefabButton);
			goButton.transform.SetParent(parentPanel, false);

			if(debugMode)
				goButton.transform.localScale = new Vector3(1, 2, 1);

			Text textButt = goButton.GetComponentInChildren<Text> ();
			textButt.text = textButt.text + c;

			Button tempButton = goButton.GetComponent<Button>();

			int tempInt;

			if (int.TryParse (c, out tempInt)) {
				tempButton.onClick.AddListener (() => ButtonClicked (tempInt));
			} else {
				Debug.Log ("conversione fallita");
			}
		}
	}

	void ButtonClicked(int buttonNo)
	{
		string nick = userNick.text;

		if (string.Equals (nick, "")) {
		
			Debug.Log ("A ZOZZO E' VUOTA");
		
		} else {

			ApplicationModel.userName = nick;

			if (ApplicationModel.offlineMode) {
				GetComponentInParent<DownloadLevelHandler>().DownloadDebugLevel();
			} else {
				GetComponentInParent<DownloadLevelHandler>().DownloadLevel(buttonNo);
			}
		}
	}
}
