﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SimpleJSON;

/*
	https://www.whatismybrowser.com/detect/what-is-my-local-ip-address

	Per i test sul telefono serve l'ip locale dopo che il telefono fa da hotspot al computer, sul quale 
	ci sono server database e tricchetracche
	
*/
public class GetUserConnection : MonoBehaviour {

	public RectTransform mainCanvas;
	public Text userNameText;
	public Fading fadingManager;

	private string currentUrl;
	private WWW www_post;

	private RectTransform canvasLoadingLogo;
	private RectTransform canvasLoadingRotella;
	private RectTransform canvasLogoCloud;
	private RectTransform canvasLogoSticks;
	private RectTransform canvasUserButton;
	private RectTransform canvasPlayButton;

	// Use this for initialization
	void Start () {

		canvasLoadingLogo = mainCanvas.Find ("LoadingLogo") as RectTransform;
		canvasLoadingRotella = mainCanvas.Find ("LoadingRotella") as RectTransform;
		canvasLogoCloud = mainCanvas.Find ("Logo_Cloud") as RectTransform;
		canvasLogoSticks = mainCanvas.Find ("Logo_Sticks") as RectTransform;
		canvasUserButton = mainCanvas.Find ("UserButton") as RectTransform;
		canvasPlayButton = mainCanvas.Find ("PlayButton") as RectTransform;

		if (ApplicationModel.userLoaded) {
		
			userNameText.text = ApplicationModel.userName;

			EnablingComponents ();

		} else {
			
			string deviceID = SystemInfo.deviceUniqueIdentifier;

			currentUrl = ApplicationModel.linkToServer + "/MindCraft/UserController/GetUser";

			WWWForm form = new WWWForm();
			form.AddField("deviceID", deviceID);
						
			www_post = new WWW (currentUrl, form);

			StartCoroutine(WaitForGettingUser(www_post));
		}
	}

	IEnumerator WaitForGettingUser(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode userNode = JSON.Parse(www.text);

			ApplicationModel.userName = userNode ["userName"];
			ApplicationModel.userGameId = userNode ["userGameId"];

			userNameText.text = ApplicationModel.userName;

			ApplicationModel.userLoaded = true;

			fadingManager.fadeSpeed = 0.8f;
			float fadeTime = fadingManager.BeginFade (1);
			yield return new WaitForSeconds(fadeTime);

			EnablingComponents ();

			fadingManager.OnLevelWasLoaded ();

		} else {

			/*
				TODO Al momento la mancanza di connessione indica che siamo da Telefono, però deve uscire un avviso 
				perchè senza connessione non si può giocare.
			*/
			Debug.Log (www.error);
		
			ApplicationModel.userName = "Player_Debug";
			ApplicationModel.userGameId = "0000000";

			ApplicationModel.serverOffline = true;

			userNameText.text = ApplicationModel.userName;
			ApplicationModel.userLoaded = true;

			fadingManager.fadeSpeed = 0.8f;
			float fadeTime = fadingManager.BeginFade (1);
			yield return new WaitForSeconds(fadeTime);

			EnablingComponents ();
		
			fadingManager.OnLevelWasLoaded ();
		}
	}

	private void EnablingComponents()
	{
		canvasLoadingLogo.gameObject.SetActive (false);

		canvasLoadingRotella.position = new Vector3 (canvasLoadingRotella.position.x, -1.15625f, canvasLoadingRotella.position.z);
				
		canvasLogoCloud.gameObject.SetActive (true);
		canvasLogoSticks.gameObject.SetActive (true);
		canvasUserButton.gameObject.SetActive (true);
		canvasPlayButton.gameObject.SetActive (true);
	}

}
