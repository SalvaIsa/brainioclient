﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class GetLeaderboardConnection : MonoBehaviour {

	private string currentUrl;
	private WWW www_post;

	public RectTransform boardPanel;
	public GameObject prefabLabel;
	public InputField level;
	public InputField ratio;
	
	public void getLeaderboards () {

		currentUrl = "http://localhost:8080/MindCraft/ScoreController/GetBoard";

		WWWForm form = new WWWForm();
		form.AddField("level", level.text);

		www_post = new WWW (currentUrl, form);

		StartCoroutine(WaitForGettingLeaderboard(www_post));

	}

	public void getRatioMask () {
	
		currentUrl = "http://localhost:8080/MindCraft/BestController/ComputeBest";

		WWWForm form = new WWWForm();
		form.AddField("level", level.text);
		form.AddField("threshold", ratio.text);

		www_post = new WWW (currentUrl, form);

		StartCoroutine(WaitForGettingRatios(www_post));

	}

	IEnumerator WaitForGettingLeaderboard(WWW www)
	{
		yield return www;

		if (www.error == null) {
			
			Debug.Log (www.text);

			boardPanel.DetachChildren ();

			JSONNode node = JSON.Parse(www.text);
			JSONArray scores = node.AsArray;

			Debug.Log("# di punteggi: " + scores.Count);

			for (int i = scores.Count - 1; i >= 0; i--) {
			
				Debug.Log (scores[i]);

				GameObject goButton = (GameObject)Instantiate(prefabLabel);
				goButton.transform.SetParent(boardPanel, false);
				goButton.transform.localScale = new Vector3(1, 2, 1);

				Text textButt = goButton.GetComponentInChildren<Text> ();
				textButt.text = scores[i]["user"] + " " + scores[i]["score"];

				Button tempButton = goButton.GetComponent<Button>();
				tempButton.interactable = false;

			}

		} else {
			Debug.Log (www.error);		
		}
	}

	IEnumerator WaitForGettingRatios(WWW www)
	{	
		yield return www;

		if (www.error == null) {
		
			Debug.Log (www.text);

		} else {
			Debug.Log (www.error);
		}
	}
}
