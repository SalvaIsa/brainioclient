﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class ScoringHandler : MonoBehaviour {

	public Text firstPlayerText;
	public Text secondPlayerText;
	public Text thirdPlayerText;
	public bool scoreDebugMode;

	private string currentUrl;
	private WWW www_post;

	private AnimateScoreMenu animScoreMenu;
	private IncreasingSlider increSlider;

	void Start () {

		increSlider = GetComponent<IncreasingSlider> ();
		animScoreMenu = GetComponent<AnimateScoreMenu> ();

		firstPlayerText.text = ApplicationModel.userName;
		secondPlayerText.text = ApplicationModel.firstTeamMate;
		thirdPlayerText.text = ApplicationModel.secondTeamMate;

		if (!scoreDebugMode) {
		
			//GetMatchScore ();
		
			SaveSelections ();

		} else
			SetFakeScores ();
	}

	private void HandleJson()
	{
		string text = "[{\"totalDiceScore\":5.6512947,\"firstPlayerPoints\":10,\"secondPlayerPoints\":10,\"thirdPlayerPoints\":2},{\"totalDiceScore\":5.6512947,\"firstPlayerPoints\":10,\"secondPlayerPoints\":10,\"thirdPlayerPoints\":2}]";
		JSONNode result = JSON.Parse(text);

		JSONNode firstTeam = JSON.Parse(result.AsArray [0].ToString());
		JSONNode secondTeam = JSON.Parse(result.AsArray [1].ToString());
	}

	public void SaveSelections()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/ScoreController/SaveSelections";

		WWWForm form = new WWWForm();

		form.AddField("matchId", ApplicationModel.matchId);

		form.AddField ("user", ApplicationModel.userName);

		form.AddField ("sliceCount", ApplicationModel.slicesCount);
		form.AddField ("level", ApplicationModel.level);

		for (int i = 0; i < ApplicationModel.slicesCount; i++) 
		{
			// 	Indices
				form.AddField("valid " + i, ApplicationModel.sliceValidities[i] + "");				

			// Damages
			string res = "";

			foreach(string s in ApplicationModel.GetDamagesList(i))
			{
				res += s + "*";
			}

			form.AddField("dmg " + i, res);				
		}

		www_post = new WWW (currentUrl, form);
		StartCoroutine(WaitForSavingResults(www_post));
	}

	IEnumerator WaitForSavingResults(WWW www)
	{
		yield return www;

		if (www.error == null) {

			CheckEverybodySaved ();

		} else {
		
			Debug.Log (www.error);
			SetFakeScores ();
		}
	}

	public void CheckEverybodySaved()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/ScoreController/CheckSaved";

		WWWForm form = new WWWForm();
		form.AddField("matchId", ApplicationModel.matchId);

		www_post = new WWW (currentUrl, form);
		StartCoroutine(WaitForCheckingSaved(www_post));
	}

	IEnumerator WaitForCheckingSaved(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode node = JSON.Parse(www.text);

			bool finished = node ["content"].AsBool;

			if (finished) {
			
				animScoreMenu.UpdateCircleSprite ();

				if (ApplicationModel.amIcreator) {
				
					CreateMatchScore ();

				} else {
					
					CheckGameOver ();
				}
			} else {
			
				Debug.Log ("Ancora no, aspè");
				CheckEverybodySaved ();
			}

		} else {

			Debug.Log (www.error);
		}
	}

	public void CreateMatchScore()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/ScoreController/CreateMatchScore";

		WWWForm form = new WWWForm();

		form.AddField("matchId", ApplicationModel.matchId);
		form.AddField("waitingId", ApplicationModel.waitingId);

		form.AddField ("sliceCount", ApplicationModel.slicesCount);
		form.AddField("level", ApplicationModel.level);

		for (int i = 0; i < ApplicationModel.slicesCount; i++) 
		{
			string res = "";

			res +=	ApplicationModel.GetDamagesList (i).ElementAt (0);

			form.AddField("dmg " + i, res);				
		}

		www_post = new WWW (currentUrl, form);
		StartCoroutine(WaitForCreatingScore(www_post));
	}
		
	IEnumerator WaitForCreatingScore(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode node = JSON.Parse(www.text);

			int id = node ["id"].AsInt;

			if (id == 42) {
				
				Debug.Log ("Ora prendo lo Score");

				GetMatchScore ();

			} else {

				Debug.Log ("Errore da qualche parte");
			}

		} else {

			Debug.Log (www.error);
		}	
	}

	public void GetMatchScore()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/ScoreController/GetMatchScore";

		WWWForm form = new WWWForm();
		form.AddField("matchId", ApplicationModel.matchId);

		www_post = new WWW (currentUrl, form);
		StartCoroutine(WaitForGettingScore(www_post));
	}

	IEnumerator WaitForGettingScore(WWW www)
	{
		Debug.Log ("everitingsfine");

		yield return www;
		
		if (www.error == null) {

			while (!www.isDone) {
				Debug.Log ("scoring.." + www.progress);
			}

			Debug.Log("Final WWW Text: "+ www.text);

			JSONNode result = JSON.Parse(www.text);

			float firstDice = result["firstDice"].AsFloat;
			float secondDice = result["oppDice"].AsFloat;

			int firstUserPoints;
			int secondUserPoints;
			int thirdUserPoints;

			if (ApplicationModel.belogToFirstTeam) {
							
				firstUserPoints = result ["firstPlayerPoints"].AsInt;
				secondUserPoints = result ["secondPlayerPoints"].AsInt;
				thirdUserPoints = result ["thirdPlayerPoints"].AsInt;
							
			} else {
				
				firstUserPoints = result ["firstOppPoints"].AsInt;
				secondUserPoints = result ["secondOppPoints"].AsInt;
				thirdUserPoints = result ["thirdOppPoints"].AsInt;
				
			}

			firstDice = Mathf.Round(firstDice * 10f) / 100f;
			secondDice = Mathf.Round(secondDice * 10f) / 100f;
			
			Debug.Log("First Dice after Round = " + firstDice);
			Debug.Log("Second Dice after Round = " + secondDice);

			increSlider.valueOne = firstDice;
			increSlider.valueTwo = secondDice;
			
			increSlider.playerValueOne =  ((float) firstUserPoints / (float)ApplicationModel.slicesCount);
			increSlider.playerValueTwo = ((float) secondUserPoints / (float)ApplicationModel.slicesCount);
			increSlider.playerValueThree = ((float) thirdUserPoints / (float)ApplicationModel.slicesCount);
			
			if(ApplicationModel.belogToFirstTeam)
				animScoreMenu.SetSpriteToResult (increSlider.valueOne > increSlider.valueTwo);
			else
				animScoreMenu.SetSpriteToResult (increSlider.valueTwo >  increSlider.valueOne);
						
			animScoreMenu.StopFirstCircle ();
			
			Debug.Log ("1 player pts " + increSlider.playerValueOne);
			Debug.Log ("2 player pts " + increSlider.playerValueTwo);
			Debug.Log ("3 player pts " + increSlider.playerValueThree);


//			JSONNode firstTeam = JSON.Parse(result.AsArray [0].ToString());
//			JSONNode secondTeam = JSON.Parse(result.AsArray [1].ToString());
//
//			float firstDice = firstTeam["totalDiceScore"].AsFloat;
//			float secondDice = secondTeam["totalDiceScore"].AsFloat;
//
//			int firstUserPoints;
//			int secondUserPoints;
//			int thirdUserPoints;
//
//			if (ApplicationModel.belogToFirstTeam) {
//			
//				firstUserPoints = firstTeam ["firstPlayerPoints"].AsInt;
//				secondUserPoints = firstTeam ["secondPlayerPoints"].AsInt;
//				thirdUserPoints = firstTeam ["thirdPlayerPoints"].AsInt;
//			
//			} else {
//
//				firstUserPoints = secondTeam ["firstPlayerPoints"].AsInt;
//				secondUserPoints = secondTeam ["secondPlayerPoints"].AsInt;
//				thirdUserPoints = secondTeam ["thirdPlayerPoints"].AsInt;
//
//			}
//		
//			firstDice = Mathf.Round(firstDice * 10f) / 100f;
//			secondDice = Mathf.Round(secondDice * 10f) / 100f;
//
//			Debug.Log ("");
//			Debug.Log("First Dice after Round = " + firstDice);
//			Debug.Log("Second Dice after Round = " + secondDice);
//
//			increSlider.valueOne = firstDice;
//			increSlider.valueTwo = secondDice;
//
//			increSlider.playerValueOne =  ((float) firstUserPoints / (float)ApplicationModel.slicesCount);
//			increSlider.playerValueTwo = ((float) secondUserPoints / (float)ApplicationModel.slicesCount);
//			increSlider.playerValueThree = ((float) thirdUserPoints / (float)ApplicationModel.slicesCount);
//
//			if(ApplicationModel.belogToFirstTeam)
//				animScoreMenu.SetSpriteToResult (increSlider.valueOne > increSlider.valueTwo);
//			else
//				animScoreMenu.SetSpriteToResult (increSlider.valueTwo >  increSlider.playerValueOne);
//			
//			animScoreMenu.StopFirstCircle ();
//
//			Debug.Log ("1 player pts " + increSlider.playerValueOne);
//			Debug.Log ("2 player pts " + increSlider.playerValueTwo);
//			Debug.Log ("3 player pts " + increSlider.playerValueThree);

		} else {
			
			Debug.Log("WWW Error: "+ www.error);

			SetFakeScores ();
		}
	}

	public void CheckGameOver()
	{
		currentUrl = ApplicationModel.linkToServer + "/MindCraft/ScoreController/CheckGameOver";

		WWWForm form = new WWWForm();
		form.AddField("matchId", ApplicationModel.matchId);

		www_post = new WWW (currentUrl, form);
		StartCoroutine(WaitForCheckingGameOver(www_post));
	}

	IEnumerator WaitForCheckingGameOver(WWW www)
	{
		yield return www;

		if (www.error == null) {

			JSONNode node = JSON.Parse(www.text);

			bool finished = node ["content"].AsBool;

			if (finished) {
				
				GetMatchScore ();

			} else {

				Debug.Log ("Ti ho detto di no...");
				CheckGameOver ();
			}

		} else {

			Debug.Log (www.error);
		}
	}

	private void SetFakeScores()
	{
		increSlider.valueOne = 0.78f;
		increSlider.valueTwo = 0.42f;

		increSlider.playerValueOne = 0.3f;
		increSlider.playerValueTwo = 0.7f;
		increSlider.playerValueThree = 0.2f;

		animScoreMenu.SetSpriteToResult (increSlider.valueOne > increSlider.valueTwo);
		animScoreMenu.StopFirstCircle ();
	}
			
//	Esempio di conversione in ArrayList di elementi del JSON
//
//	for (int n = 0; n < node ["scores"].AsArray.Count; n++) {
//
//		float score = node ["scores"] [n] ["fmeasure"].AsFloat;
//
//		if (float.IsNaN (score)) {
//
//			Debug.Log (" trovato un NAN ");
//
//		} else {
//
//			Debug.Log("slice # " + n + " score = " + score);
//		}
//	}

	public void UNUSED_SaveScore()
	{
		currentUrl = "http://localhost:8080/MindCraft/ScoreController/SaveScore";

		WWWForm form = new WWWForm();
		form.AddField("user", ApplicationModel.userName);
		form.AddField("level", ApplicationModel.level);

		www_post = new WWW (currentUrl, form);

		StartCoroutine(UNUSED_WaitForSavingScore(www_post));
	}

	IEnumerator UNUSED_WaitForSavingScore(WWW www)
	{
		yield return www;

		if (www.error == null) {
		
			Debug.Log("WWW Text: "+ www.text);

			ApplicationModel.CleanModel ();

			SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
			
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}
	}

}
