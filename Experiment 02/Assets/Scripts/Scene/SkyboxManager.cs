﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxManager : MonoBehaviour {

	private float dayFactor = 1.0f;
	private float nightFactor = 0.0f;

	public float blendingFactor;
	public float blendingSpeed;

	public bool toNight;

	void Update()
	{
		updateSkybox ();	
	}

	public void updateSkybox()
	{
		if (toNight) {
	
			if (blendingFactor == 1.0f)
				return;
			
			blendingFactor += blendingSpeed * Time.deltaTime;

			if (blendingFactor > 1.0f)
				blendingFactor = 1.0f;
		}
		else {
		
			if (blendingFactor == 0.0f)
				return;

			blendingFactor -= blendingSpeed * Time.deltaTime;

			if (blendingFactor < 0.0f)
				blendingFactor = 0.0f;
		}

		RenderSettings.skybox.SetFloat ("_Blend", blendingFactor);
	}

	public void DayToNight()
	{
		toNight = true;
	}

	public void NightToDay()
	{
		toNight = false;		
	}

}
