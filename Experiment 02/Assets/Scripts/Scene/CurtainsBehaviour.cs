﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurtainsBehaviour : MonoBehaviour {

	private RectTransform leftCurtain;
	private RectTransform rightCurtain;

	public RectTransform leftOpenTarget;
	public RectTransform rightOpenTarget;
	public RectTransform leftCloseTarget;
	public RectTransform rightCloseTarget;

	public GameObject mainCanvas;
	public GameObject playCanvas;
	public SkyboxRotator skyRotator;

	public bool opening;
	public bool closing;
	public float speed;

	// Use this for initialization
	void Start () {

		leftCurtain = transform.Find ("LeftCurtain").gameObject.GetComponent<RectTransform>();
		rightCurtain = transform.Find ("RightCurtain").gameObject.GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {

		if (closing) {
		
			leftCurtain.position = Vector3.MoveTowards (leftCurtain.position, leftCloseTarget.position, speed);
			rightCurtain.position = Vector3.MoveTowards (rightCurtain.position, rightCloseTarget.position, speed);

			if (leftCurtain.position.Equals (leftCloseTarget.position)
				&& rightCurtain.position.Equals (rightCloseTarget.position)) {
			
				mainCanvas.SetActive (false);
				playCanvas.SetActive (true);
				skyRotator.setNightSky ();

				opening = true;
				closing = false;
			}

		} else if (opening) {

			leftCurtain.position = Vector3.MoveTowards (leftCurtain.position, leftOpenTarget.position, speed);
			rightCurtain.position = Vector3.MoveTowards (rightCurtain.position, rightOpenTarget.position, speed);

			if (leftCurtain.position.Equals (leftOpenTarget.position)
				&& rightCurtain.position.Equals (rightOpenTarget.position)) {

				opening = false;
			}
		}
	}

	public void CloseCurtains()
	{
		opening = false;
		closing = true;
	}

	public void OpenCurtains()
	{
		closing = false;
		opening = true;
	}


}
