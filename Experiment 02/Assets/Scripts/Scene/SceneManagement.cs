﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManagement : MonoBehaviour {

	public string m_biasTextures;
	public string m_visorTextures;
	public string m_clusteredTextures;
	public string m_superpixTextures;
	public string m_overlayTextures;

	public RectTransform mainCanvas;
	public RectTransform matchCanvas;

	public Button[] myTeamButtons;
	public Text[] myTeamTexts;
	public Button[] oppoTeamButtons;
	public Text[] oppoTeamTexts;

	public Sprite voidTeamSprite;
	public Sprite myPlayerSprite;
	public Sprite myTeamSprite;
	public Sprite oppoTeamSprite;

	private Fading fadingManager;

	void Start () {

		fadingManager = GetComponent<Fading> ();
	}

	public void AddPlayerDirectly(string userNick, bool friend, int position)
	{
		if (friend) {
		
			myTeamTexts [position].text = userNick;
			myTeamButtons [position].image.sprite = myTeamSprite;

			if (userNick.Equals (ApplicationModel.userName)) {

				ApplicationModel.belogToFirstTeam = true;
				myTeamButtons [position].image.sprite = myPlayerSprite;

			}
			else
				myTeamButtons [position].image.sprite = myTeamSprite;

		} else {
		
			oppoTeamTexts [position].text = userNick;
			oppoTeamButtons [position].image.sprite = oppoTeamSprite;

			if (userNick.Equals (ApplicationModel.userName)) {

				ApplicationModel.belogToFirstTeam = false;
				oppoTeamButtons [position].image.sprite = myPlayerSprite;

			} else {
			
				oppoTeamButtons [position].image.sprite = oppoTeamSprite;
			}
		}
	}

	public void AddPlayer(string userNick, bool friend)
	{
		if (friend) {
		
			if (myTeamTexts [1].text.Equals ("- - -")) {
			
				myTeamTexts [1].text = userNick;
				myTeamButtons [1].image.sprite = myTeamSprite;

			} else if (myTeamTexts [2].text.Equals ("- - -")) {

				myTeamTexts [2].text = userNick;
				myTeamButtons [2].image.sprite = myTeamSprite;
			}
				
		} else {
		
			if (oppoTeamTexts [0].text.Equals ("- - -")) {

				oppoTeamTexts [0].text = userNick;
				oppoTeamButtons [0].image.sprite = oppoTeamSprite;

			} else if (oppoTeamTexts [1].text.Equals ("- - -")) {

				oppoTeamTexts [1].text = userNick;
				oppoTeamButtons [1].image.sprite = oppoTeamSprite;

			} else if (oppoTeamTexts [2].text.Equals ("- - -")){

				oppoTeamTexts [2].text = userNick;
				oppoTeamButtons [2].image.sprite = oppoTeamSprite;
			}

		}
	}

	public string[] GetTeamMates()
	{
		string[] mates = new string[2];

		if (ApplicationModel.belogToFirstTeam) {
		
			if (myTeamTexts [0].text.Equals (ApplicationModel.userName)) {

				mates [0] = myTeamTexts [1].text;
				mates [1] = myTeamTexts [2].text;

			} else if (myTeamTexts [1].text.Equals (ApplicationModel.userName)) {

				mates [0] = myTeamTexts [0].text;
				mates [1] = myTeamTexts [2].text;

			} else if (myTeamTexts [2].text.Equals (ApplicationModel.userName)){

				mates [0] = myTeamTexts [0].text;
				mates [1] = myTeamTexts [1].text;
			}

		} else {
		
			if (oppoTeamTexts [0].text.Equals (ApplicationModel.userName)) {

				mates [0] = oppoTeamTexts [1].text;
				mates [1] = oppoTeamTexts [2].text;

			} else if (oppoTeamTexts [1].text.Equals (ApplicationModel.userName)) {

				mates [0] = oppoTeamTexts [0].text;
				mates [1] = oppoTeamTexts [2].text;

			} else if (oppoTeamTexts [2].text.Equals (ApplicationModel.userName)){

				mates [0] = oppoTeamTexts [0].text;
				mates [1] = oppoTeamTexts [1].text;
			}
		}

		return mates;
	}

	public void ShowMatchmaking()
	{
		mainCanvas.gameObject.SetActive (false);
		matchCanvas.gameObject.SetActive (true);

		if (ApplicationModel.serverOffline) {
			myTeamTexts [0].text = ApplicationModel.userName;
			myTeamButtons [0].image.sprite = myTeamSprite;
		}
	}

	public void ShowMainCanvas()
	{
		mainCanvas.gameObject.SetActive (true);
		matchCanvas.gameObject.SetActive (false);

		ResetTextFields ();
	}
		
	public void GoToGameScene()
	{
		//Setting Teammates and Opponents
		if (ApplicationModel.serverOffline) {
			
			ApplicationModel.firstTeamMate = myTeamTexts[1].text;
			ApplicationModel.secondTeamMate = myTeamTexts[2].text;
		}

		//	Bias and Visors belong to Image #3
		ApplicationModel.level = 3;
		
		ApplicationModel.biasTextures = Resources.LoadAll(m_biasTextures, typeof(Texture2D));
		ApplicationModel.visorTextures = Resources.LoadAll(m_visorTextures, typeof(Texture2D));
		ApplicationModel.clusteredTextures = Resources.LoadAll(m_clusteredTextures, typeof(Texture2D));
		ApplicationModel.superpixelTextures = Resources.LoadAll(m_superpixTextures, typeof(Texture2D));
		ApplicationModel.overlayTextures = Resources.LoadAll(m_overlayTextures, typeof(Texture2D));

		//		ApplicationModel.slicePath = imagePath;
		ApplicationModel.slicesCount = ApplicationModel.biasTextures.Length;

		StartCoroutine(FadingToGameScene());

	}

	private void ResetTextFields()
	{
		for(int i = 0; i < oppoTeamTexts.Length; i++)
			oppoTeamTexts [i].text = "- - -";

		for(int i = 0; i < myTeamTexts.Length; i++)
			myTeamTexts [i].text = "- - -";

		for(int i = 0; i < oppoTeamButtons.Length; i++)
			oppoTeamButtons [i].image.sprite = voidTeamSprite;

		for(int i = 1; i < myTeamButtons.Length; i++)
			myTeamButtons [i].image.sprite = voidTeamSprite;

	}

	IEnumerator FadingToGameScene(bool value)
	{
		float fadeTime = fadingManager.BeginFade (1);
		yield return new WaitForSeconds(fadeTime);

		mainCanvas.gameObject.SetActive (!value);
		matchCanvas.gameObject.SetActive (value);

		fadingManager.OnLevelWasLoaded ();
	}

	IEnumerator FadingToGameScene()
	{
		float fadeTime = fadingManager.BeginFade (1);
		yield return new WaitForSeconds(fadeTime);

		fadingManager.OnLevelWasLoaded ();
		LoadingScreenManager.LoadScene (2);
	}
}
