﻿using UnityEngine;

public class SkyboxRotator : MonoBehaviour
{
	public Material daySkybox;
	public Material nightSkybox;

    public float RotationPerSecond = 1;

    private bool _rotate = true;

    protected void Update()
    {
        if (_rotate) RenderSettings.skybox.SetFloat("_Rotation", Time.time * RotationPerSecond);
    }

    public void ToggleSkyboxRotation()
    {
        _rotate = !_rotate;
    }
		
	public void setDaySky()
	{
		RenderSettings.skybox = daySkybox;
	}

	public void setNightSky()
	{
		RenderSettings.skybox = nightSkybox;		
	}

}