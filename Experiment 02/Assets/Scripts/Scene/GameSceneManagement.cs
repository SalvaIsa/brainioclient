﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameSceneManagement : MonoBehaviour {

	[SerializeField] Camera gameCamera;
	[SerializeField] Image timerImageFilled;
	[SerializeField] Image sciIndexImageFilled;
	[SerializeField] Image zawarudoImage;
	[SerializeField] Image zawarudoImageFilled;
	[SerializeField] Sprite zawarudoClicked;
	[SerializeField] Image drawingImage;
	[SerializeField] Image drawingImageFilled;
	[SerializeField] Sprite drawingClicked;
	[SerializeField] RectTransform downSelectionImage;
	private Sprite zawarudoNormal;
	private Sprite drawingNormal;

	// Use this for initialization
	void Start () {

		zawarudoNormal = zawarudoImageFilled.sprite;
		drawingNormal = drawingImageFilled.sprite;
	}

	public void ShrinkZawarudoImage()
	{
		zawarudoImageFilled.sprite = zawarudoClicked;
		zawarudoImage.rectTransform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
	}

	public void ShrinkDrawingImage()
	{
		drawingImageFilled.sprite = drawingClicked;
		drawingImage.rectTransform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
	}

	public void SetNormalZaWarudoImage()
	{
		zawarudoImageFilled.sprite = zawarudoNormal;
	}

	public void SetNormalDrawingImage()
	{
		drawingImageFilled.sprite = drawingNormal;
	}

	public void EnlargeZawarudoImage()
	{
		zawarudoImage.rectTransform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
	}

	public void EnlargeDrawingImage()
	{
		drawingImage.rectTransform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
	}

	public void FillZawarudoImage(float amount)
	{
		zawarudoImageFilled.fillAmount = amount;
	}

	public void FillTimerImage(float amount)
	{
		timerImageFilled.fillAmount = amount;
	}

	public void MoveSelecButton(bool toLeft)
	{
		if (toLeft) {
		
			downSelectionImage.transform.DOLocalMoveX (-300, 0.5f, false);

		} else {
		
			downSelectionImage.transform.DOLocalMoveX (0, 0.6f, false);

		}
	}

	public void MoveZoomCamera(Vector3 toPosition, float toFov)
	{
		gameCamera.transform.DOMove (toPosition, 0.6f, false);
		DOTween.To(()=> gameCamera.fieldOfView, x=> gameCamera.fieldOfView = x, toFov, 0.6f);
	}

	public void FillSciIndex(float val)
	{
		DOTween.To(()=> sciIndexImageFilled.fillAmount, x=> sciIndexImageFilled.fillAmount = x, val, 0.6f);
	}
}
