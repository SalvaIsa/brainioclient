﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyHistogram : MonoBehaviour {

	public Texture2D wholeInputTexture;
	public int[] wholeHistogramData;
	public bool work;

	public Texture2D inputTexture;   
	public int[] histogramData;
	public int[] divergenceData;

	public float regionMean;
	private float floatThreshold;

	void Update()
	{
		if(work)
		{
			wholeHistogramData =  new int[101];

			Color[] pixels = wholeInputTexture.GetPixels(0);

			for (int i = 0; i < pixels.Length; i++) 
			{
				if (pixels [i] != Color.black) {

					float originalValue = pixels [i].grayscale;
					originalValue = Mathf.Round(originalValue * 100f) / 100f;

					int histoValue = (int)(originalValue * 100);

					wholeHistogramData [histoValue] += 1;
				}
			}
				
			int histoMax = 0;
			int histoIndexMax = 0;

			for (int j = 0; j < wholeHistogramData.Length; j++) {

				if (wholeHistogramData [j] > histoMax) {

					histoMax = wholeHistogramData [j];
					histoIndexMax = j;
				}
			}
			Debug.Log ("WHOLE Histogram Peek = " + histoMax + " valore = " + histoIndexMax); 

			work = false;
		}
	}

	public void ProvaDue_SetParameters()
	{
		inputTexture = GetComponent<MeshRenderer>().material.mainTexture as Texture2D;
		histogramData =  new int[101];
		regionMean = 0.0f;

		Color[] pixels = inputTexture.GetPixels(0);

		/*
			Histogram 
		*/
		for (int i = 0; i < pixels.Length; i++) {

			float originalValue = pixels [i].grayscale;
			originalValue = Mathf.Round(originalValue * 100f) / 100f;

			int histoValue = (int)(originalValue * 100);
//			Debug.Log ("original " + originalValue);
//			Debug.Log ("int " + histoValue);

			histogramData [histoValue] += 1;
		}

		/*
			Histogram Max Value
		*/
		int histoMax = 0;
		int histoIndexMax = 0;

		for (int j = 0; j < histogramData.Length; j++) {

			if (histogramData [j] > histoMax) {

				histoMax = histogramData [j];
				histoIndexMax = j;
			}
		}
		Debug.Log ("Histogram Peek = " + histoMax + " in posizione = " + histoIndexMax); 
	}

	public void SetParameters()
	{
		inputTexture = GetComponent<MeshRenderer>().material.mainTexture as Texture2D;
		histogramData =  new int[100];
		divergenceData =  new int[100];
		regionMean = 0.0f;

		Color[] pixels = inputTexture.GetPixels(0);

		/*
			Histogram 
		*/
		for (int i = 0; i < pixels.Length; i++) {
		
			float originalValue = pixels [i].grayscale;
			originalValue = Mathf.Round(originalValue * 100f) / 100f;

			int histoValue = (int)(originalValue * 100);

			histogramData [histoValue] += 1;
		}
			
		/*
			Divergence Diagram
		*/	
		for (int d = 0; d < divergenceData.Length; d++) {
		
			if (d + 1 < divergenceData.Length)
				divergenceData [d] = histogramData [d] - histogramData [d + 1];
			else
				divergenceData [d] = histogramData [d];
		}

		/*
			Divergence Max Value
		*/
		int diverMax = 0;
		int diverIndiceMax = 0;
		
		for (int j = 0; j < divergenceData.Length; j++) {
			
			if (divergenceData [j] > diverMax) {
				
				diverMax = divergenceData [j];
				diverIndiceMax = j;
			}
		}
		Debug.Log ("Diver Max = " + diverMax + " in posizione = " + diverIndiceMax); 

		/*
			Optimal Threshold
		*/
		int optiThres = diverMax;
		int optiIndiceThres = diverIndiceMax;

		for (int jj = diverIndiceMax+1; jj < divergenceData.Length; jj++)
		{
			if (divergenceData [jj] < optiThres && divergenceData [jj] > 0) {
		
				optiThres = divergenceData [jj];
				optiIndiceThres = jj;
			} else if (divergenceData [jj] == 0) {
				break;
			}
		}
		Debug.Log ("Opti Threshold = 0." + optiIndiceThres + " con divergenza= " + optiThres); 

		/*
			Getting Seeds 
		*/
		List<Vector2> seeds = new List<Vector2>();
		floatThreshold = (float)optiIndiceThres / 100.0f;

		for (int y = 0; y < inputTexture.height; y++)
		{
			for (int x = 0; x < inputTexture.width; x++)
			{
				int index = (y * inputTexture.width + x);

				float chomped = Mathf.Round(pixels [index].grayscale * 100f) / 100f;

				if (pixels [index].grayscale > floatThreshold)
					seeds.Add (new Vector2 (x,y));
			}
		}

		/*
			Region Growing
		*/
//			Debug.Log ("# of Seeds = " + seeds.Count);
//			foreach( Vector2 seed in seeds)
//			{
//				Color seedCol = inputTexture.GetPixel ((int)seed.x, (int)seed.y);
//	
//				if (!seedCol.Equals (Color.red) && !seedCol.Equals (Color.blue)) {
//				
//					regionMean = seedCol.grayscale;
//	
//					inputTexture.SetPixel ((int)seed.x, (int)seed.y, Color.blue);
//					inputTexture.Apply ();
//	
//					RegionGrowing ((int)seed.x, (int)seed.y);
//				}
//			}

	}

	public void RegionGrowing(int piX, int piY)
	{
		Debug.Log ("---");
		Debug.Log("Float Optimal Threshold " + floatThreshold);
		Debug.Log("Region Mean " + regionMean);

		//RIGHT
		if (piX + 1 < inputTexture.width) {
			
			Color rightColor = inputTexture.GetPixel (piX+1, piY);

			if (!rightColor.Equals (Color.red) && !rightColor.Equals (Color.blue)) {

				float toAddDifference = Mathf.Abs (rightColor.grayscale - regionMean);
				float comparingDifference = Mathf.Abs (regionMean - floatThreshold);

				toAddDifference = Mathf.Round(toAddDifference * 1000f) / 1000f;
				comparingDifference = Mathf.Round(comparingDifference * 1000f) / 1000f;

				if (toAddDifference < comparingDifference) {

					regionMean = (regionMean + rightColor.grayscale) / 2.0f;

					inputTexture.SetPixel (piX + 1, piY, Color.red);
					inputTexture.Apply ();

					RegionGrowing (piX+1, piY);
				}
			}
		}

		//LEFT
		if (piX - 1 >= 0) {

			Color leftColor = inputTexture.GetPixel (piX-1, piY);

			if (!leftColor.Equals (Color.red) && !leftColor.Equals (Color.blue)) {

				float toAddDifference = Mathf.Abs (leftColor.grayscale - regionMean);
				float comparingDifference = Mathf.Abs (regionMean - floatThreshold);

				toAddDifference = Mathf.Round(toAddDifference * 1000f) / 1000f;
				comparingDifference = Mathf.Round(comparingDifference * 1000f) / 1000f;

				if (toAddDifference < comparingDifference) {

					regionMean = (regionMean + leftColor.grayscale) / 2.0f;

					inputTexture.SetPixel (piX - 1, piY, Color.red);
					inputTexture.Apply ();

					RegionGrowing (piX-1, piY);
				}
			}
		}

		//UP
		if (piY + 1 < inputTexture.height) {

			Color upColor = inputTexture.GetPixel (piX, piY+1);

			if (!upColor.Equals (Color.red) && !upColor.Equals (Color.blue)) {

				float toAddDifference = Mathf.Abs (upColor.grayscale - regionMean);
				float comparingDifference = Mathf.Abs (regionMean - floatThreshold);

				toAddDifference = Mathf.Round(toAddDifference * 1000f) / 1000f;
				comparingDifference = Mathf.Round(comparingDifference * 1000f) / 1000f;

				if (toAddDifference < comparingDifference) {

					regionMean = (regionMean + upColor.grayscale) / 2.0f;

					inputTexture.SetPixel (piX, piY+1, Color.red);
					inputTexture.Apply ();

					RegionGrowing (piX, piY+1);
				}
			}
		}

		//DOWN
		if (piY - 1 >= 0) {

			Color downColor = inputTexture.GetPixel (piX, piY-1);

			if (!downColor.Equals (Color.red) && !downColor.Equals (Color.blue)) {

				float toAddDifference = Mathf.Abs (downColor.grayscale - regionMean);
				float comparingDifference = Mathf.Abs (regionMean - floatThreshold);

				toAddDifference = Mathf.Round(toAddDifference * 1000f) / 1000f;
				comparingDifference = Mathf.Round(comparingDifference * 1000f) / 1000f;

				if (toAddDifference < comparingDifference) {

					regionMean = (regionMean + downColor.grayscale) / 2.0f;

					inputTexture.SetPixel (piX, piY-1, Color.red);
					inputTexture.Apply ();

					RegionGrowing (piX, piY-1);
				}
			}
	}
}

}