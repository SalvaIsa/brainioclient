﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationModel : MonoBehaviour {

	static public string linkToServer;

	static public Object[] biasTextures;
	static public Object[] visorTextures;
	static public Object[] clusteredTextures;
	static public Object[] superpixelTextures;
	static public Object[] overlayTextures;

	static public List<string>[] modelDamagesArray;

	static public int level;
	static public string slicePath;
	static public int slicesCount;

	static public Dictionary<Color, int>[] sliceClusters;
	static public float[] sliceValidities;

	static public string userName;
	static public string userGameId;

	static public int matchId;
	static public int waitingId;
	static public bool amIcreator;
	static public bool userLoaded;

	//	TRUE -> first team
	//	FALSE -> opponent team
	static public bool belogToFirstTeam;
	static public string firstTeamMate;
	static public string secondTeamMate;

	static public bool serverOffline;

	// UNUSED
	static public bool offlineMode;
	static public List<ImageMatrix> imageMatrices;

	public static void CleanModel()
	{
		
	}

	static public List<string> GetDamagesList(int position)
	{
		return modelDamagesArray [position];
	}
		
}
