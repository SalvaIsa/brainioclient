﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerParameters : MonoBehaviour {

	[SerializeField] int layerNum;
	[SerializeField] int sliceNumber;
	[SerializeField] Texture2D clusteredTexture;
	[SerializeField] Texture2D superpixelTexture;

	public int GetLayerNum()
	{
		return layerNum;
	}

	public int GetSliceNumber()
	{
		return sliceNumber;
	}

	public void SetSliceNumber(int num)
	{
		sliceNumber = num;
	}

	public Texture2D GetClusteredTexture()
	{
		return clusteredTexture;
	}

	public void SetClusteredTexture(Texture2D txt)
	{
		clusteredTexture = txt;
	}

	public Texture2D GetSuperPixelTexture()
	{
		return superpixelTexture;
	}

	public void SetSuperPixelTexture(Texture2D txt)
	{
		superpixelTexture = txt;
	}
}
