﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionBox{

	public float onguiX;
	public float onguiY;
	public float onguiWidth;
	public float onguiHeight;

	public int cameraX;
	public int cameraY;
	public int endCameraX;
	public int endCameraY;

	public int centerCamX;
	public int centerCamY;

	public int AtapTextureX;
	public int AtapTextureY;

	public int DtapTextureX;
	public int DtapTextureY;

	public void FindBoxCenter()
	{
		centerCamX = (cameraX + endCameraX) / 2;
		centerCamY = (cameraY + endCameraY) / 2;
	}
}
