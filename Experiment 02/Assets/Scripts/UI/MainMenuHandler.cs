﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuHandler : MonoBehaviour {

	public InputField serverInput;
	public RectTransform confirmServer;
	public RectTransform loadingRotella;
	public GameObject connectionManager;

	private GetLevelsConnect allLevelsHandler;
	private GetLeaderboardConnection leaderConnection;
	private DownloadLevelHandler downLevelHandler;
	private MatchMakingConnection matchMakingConnect;

	private SkyboxManager skyManager;
	private SkyboxRotator skyRotator;

	private Fading fadingManager;
	private SceneManagement sceneManagement;

	void Start()
	{
		skyManager = GetComponent<SkyboxManager> ();
		skyRotator = GetComponent<SkyboxRotator> ();

		fadingManager = GetComponent<Fading> ();
		sceneManagement = GetComponent<SceneManagement> ();

		if (connectionManager != null) {
		
			allLevelsHandler = connectionManager.GetComponent<GetLevelsConnect> ();
			leaderConnection = connectionManager.GetComponent<GetLeaderboardConnection> ();
			downLevelHandler = connectionManager.GetComponent<DownloadLevelHandler> ();
			matchMakingConnect = connectionManager.GetComponent<MatchMakingConnection> ();
		}

	}
			
	public void PlayCallback()
	{
		Debug.Log ("Play Callback");
		sceneManagement.ShowMatchmaking ();
		matchMakingConnect.EnableSearching (true);
			
//		downLevelHandler.DownloadDebugLevel ();
//		SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
	}

	public void MatchBackCallback()
	{
		Debug.Log ("MatchBack Callback");

		if (!matchMakingConnect.IsCompleted ()) {
			matchMakingConnect.ResetFields ();
			sceneManagement.ShowMainCanvas ();
		}
	}

	public void ConfirmServerCallback()
	{
		confirmServer.gameObject.SetActive (false);
		loadingRotella.gameObject.SetActive (true);
		ApplicationModel.linkToServer = serverInput.text;

		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
	}

	public void LeaderboardCallback()
	{
		Debug.Log ("Leaderboard");
		leaderConnection.getLeaderboards ();
	}

	public void BestRatioCallback()
	{
		Debug.Log ("Best Ratio");
		leaderConnection.getRatioMask();
	}

	public void OptionsCallback()
	{
		Debug.Log ("Options");
	}

	public void QuitCallback()
	{
		Debug.Log ("Quit");
	}

}
