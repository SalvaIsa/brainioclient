﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggle : MonoBehaviour {

	public Sprite OnSprite;
	public Sprite OffSprite;

	private bool isOn;

	void Start () {

		isOn = false;
	}

	public void SwitchState()
	{
		isOn = !isOn;
		Image currSprite = GetComponent<Image> ();

		if (isOn) {		
			currSprite.sprite = OnSprite;
			//currSprite.color = new Color (0.25f, 1f, 0.3f);
		} else {
			currSprite.sprite = OffSprite;
			//currSprite.color = Color.white;
		}
	}

	public bool GetState()
	{
		return isOn;
	}
}
