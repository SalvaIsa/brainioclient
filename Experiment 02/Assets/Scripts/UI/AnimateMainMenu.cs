﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AnimateMainMenu : MonoBehaviour {

	public RectTransform mainSticks;
	public RectTransform mainRotella;
	public RectTransform matchLoadingWheel;

	private Tweener mainRotellaTweener;
	private Tweener mainstickTweener;
	private Tweener matchWheelTweener;

	void Start () {

		DOTween.Init(false, true, LogBehaviour.ErrorsOnly);

		mainRotellaTweener = matchLoadingWheel.DOLocalRotate (new Vector3 (0f, 0f, -100f), 0.8f).SetRelative().SetLoops(-1, LoopType.Incremental); 
		mainstickTweener = mainSticks.DOLocalMoveY (65f, 1f).SetRelative().SetLoops(-1, LoopType.Yoyo);
		matchWheelTweener = mainRotella.DORotate (new Vector3 (0f, 0f, 180f), 1.6f).SetLoops(-1, LoopType.Incremental);

	}

}
