﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GifComponent : MonoBehaviour {

	public Text dbg;
	public Transform background;
	public string foldName;
	public float framesPerSecond;

	private Object voidTexture;
	private Object[] allFrames;
	private bool visible;

	void Start () {

		// Loads all assets in the "Resources/xxx" folder
		// Every "Resources" folder will be read in order to find Files
		allFrames = Resources.LoadAll(foldName, typeof(Texture2D));
		voidTexture = Resources.Load("void", typeof(Texture2D));
	
		visible = false;
	}
	
	void Update () {

		if (visible) {
		
			int index = (int)(Time.time * framesPerSecond) % allFrames.Length;
			GetComponent<MeshRenderer> ().material.mainTexture = allFrames[index] as Texture;

//			if (background != null)
//				background.gameObject.SetActive (true);

		} else {
			GetComponent<MeshRenderer> ().material.mainTexture = voidTexture as Texture;

//			if (background != null)
//				background.gameObject.SetActive (false);
		}
	}

	public void SetVisible(bool val)
	{
		visible = val;
	}
}
