﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fading : MonoBehaviour {

	public Texture2D fadingTexture;
	public float fadeSpeed = 0.8f;

	private int drawDepth = -1000;
	private float alpha = 0.0f;
	private int fadeDir = -1;

	void OnGUI()
	{
		alpha += fadeDir * fadeSpeed * Time.deltaTime;
		alpha = Mathf.Clamp01 (alpha);

		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);
		GUI.depth = drawDepth;
		GUI.DrawTexture (new Rect(0, 0, Screen.width, Screen.height), fadingTexture);
	}

	// -1 = fade in, 1 = fade out
	public float BeginFade(int direction)
	{
		fadeDir = direction;
		return fadeSpeed;
	}

	// Called when the level is loaded
	public void OnLevelWasLoaded()
	{
		BeginFade (-1);
	}
}
