﻿using UnityEngine;
using System.Collections;

public class CrossFading : MonoBehaviour
{
	public  float    BlendSpeed = 3.0f;
	public bool    trigger = false;

	private float    fader = 0f;
	private MeshRenderer meshRender;

	void Start ()
	{
		meshRender = GetComponent<MeshRenderer> ();

		meshRender.material.SetFloat( "_Lerp", 0f );
	}

	void Update ()
	{
		if ( true == trigger )
		{
			fader += Time.deltaTime * BlendSpeed;

			meshRender.material.SetFloat( "_Lerp", fader );

			if ( fader >= 1.0f )
			{
				trigger = false;
				fader = 0f;
			}
		}
	}

	public void CrossFadeTo()
	{
		
	}
}