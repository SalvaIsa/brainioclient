﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenuHandler : MonoBehaviour {

	public Transform gameManager;
	public PanningZoomingCamera gameCamera;

	public ButtonToggle zawarudoButToggle;

	private LevelManager levelManager;
	private SpawningPlayer spawnPlayer;
	private ScoringHandler scoringHandler;
	private GuiBoxesMaker boxesMaker;
	private GameSceneManagement gameSceneManage;
	private PartitionedTextureClicking partTextClick;

	void Start()
	{
		boxesMaker = gameManager.GetComponent<GuiBoxesMaker> ();
		scoringHandler = gameManager.GetComponent<ScoringHandler> ();
		levelManager = gameManager.GetComponent<LevelManager> ();
		spawnPlayer = gameManager.GetComponent<SpawningPlayer> ();
		gameSceneManage = gameManager.GetComponent<GameSceneManagement> ();
		partTextClick = gameManager.GetComponent<PartitionedTextureClicking> ();
	}

	public void OnConfirmChangeSlice()
	{
		levelManager.ComputeSelectionIndex ();

		if (levelManager.IsGameOver ()) {

			levelManager.GameOverProcedure ();

		} else {

			if (gameCamera.IsMovingCompleted ()) {

				gameCamera.MoveToNextSlice ();
				levelManager.IncreaseCurrentSlice ();
				boxesMaker.UpdateTextureParameters (levelManager.GetCurrentSlice ());
				partTextClick.MovePartitionedPlane ();
				partTextClick.textPartition = boxesMaker.textPartition;
			}
		}
	}

	public void OnAfterworkDeny()
	{
		partTextClick.ResetClusterVariables ();
		partTextClick.selectionEnabled = false;
	}

	public void OnAfterworkButton()
	{
		partTextClick.ResetClusterVariables ();
		boxesMaker.GetCurrentTexturePartition().afterwork = true;
	}

	public void OnAfterworkSwitch()
	{
		boxesMaker.GetCurrentTexturePartition ().SwitchPartitionedTexture ();
	}

	public void OnClusteredSwitch(int param)
	{
		boxesMaker.GetCurrentTexturePartition ().SwitchFullTexture (param);
	}

	public void OnSelectionButton()
	{
		spawnPlayer.SetDrawingStepOne ();
	}

	public void OnButtonToggleZaWarudo()
	{
		zawarudoButToggle.SwitchState ();

		spawnPlayer.SetZaWarudoStepOne ();
	}

	public void OnImageZaWarudoPointDown()
	{
		if (!spawnPlayer.IsZaWarudoStepTwo()) {

			gameSceneManage.ShrinkZawarudoImage ();
			spawnPlayer.SetZaWarudoStepOne ();
		}
	}

	public void OnImageZaWarudoPointUp()
	{
		gameSceneManage.SetNormalZaWarudoImage ();

		//		spawnPlayer.SetZaWarudoStepOne ();
		spawnPlayer.SetZaWarudoStepTwo ();
		gameSceneManage.EnlargeZawarudoImage ();

		//		if (spawnPlayer.IsBackgroundPixel ()) {
		//		
		//			spawnPlayer.SetZaWarudoStepOne ();
		//			gameSceneManage.EnlargeZawarudoImage ();
		//
		//		}else if (!spawnPlayer.IsZaWarudoStepTwo()) {
		//
		//			spawnPlayer.SetZaWarudoStepTwo ();
		//		}

		spawnPlayer.ResetCurrentDraggingColor ();
	}

	public void OnImageDrawingPointDown()
	{
		gameSceneManage.ShrinkDrawingImage ();
		gameSceneManage.MoveSelecButton (true);

		spawnPlayer.SetDrawingStepOne ();
	}

	public void OnImageDrawingPointUp()
	{
		gameSceneManage.SetNormalDrawingImage ();
		gameSceneManage.EnlargeDrawingImage ();
		gameSceneManage.MoveSelecButton (false);

		if (!spawnPlayer.IsBackgroundPixel ()) {
		
			partTextClick.ResettingPlaneTextures ();
		}

		spawnPlayer.SetDrawingStepOne ();
		spawnPlayer.ResetCurrentDraggingColor ();

	}

	public Text dbgText;
	public Button drawButt;
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.C)) {

			drawButt.onClick.Invoke ();
		}
	}

	public void OnDrawingButtonClicked()
	{
		if (!spawnPlayer.IsBackgroundPixel ()) {
		
			if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {

				partTextClick.StartColoringProcedure (true, Input.GetTouch (0).position);
			
			} else {
				
				partTextClick.StartColoringProcedure (true, new Vector2(Input.mousePosition.x, Input.mousePosition.y));

			}
		}

//		spawnPlayer.SetDrawingStepOne ();
//		spawnPlayer.ResetCurrentDraggingColor ();
	}
}
