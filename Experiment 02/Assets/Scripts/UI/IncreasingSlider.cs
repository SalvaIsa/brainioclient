﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncreasingSlider : MonoBehaviour {

	[SerializeField] Image atrasTeamOne;
	[SerializeField] Image atrasTeamTwo;
	[SerializeField] Image atrasPlayerOne;
	[SerializeField] Image atrasPlayerTwo;
	[SerializeField] Image atrasPlayerThree;

	public float valueOne;
	public float valueTwo;
	public float playerValueOne;	
	public float playerValueTwo;
	public float playerValueThree;

	public bool activateTeams;
	public bool activatePlayers;

	private Image sliderTeamOne;
	private Image sliderTeamTwo;
	private Text textTeamOne;
	private Text textTeamTwo;

	private Image sliderPlayerOne;
	private Image sliderPlayerTwo;
	private Image sliderPlayerThree;
	private Text textPlayerOne;
	private Text textPlayerTwo;
	private Text textPlayerThree;

	private float currentValueOne;
	private float currentValueTwo;

	private float currentPlayerOne;
	private float currentPlayerTwo;
	private float currentPlayerThree;

	private AnimateScoreMenu animScoreMenu;

	void Start () {

		animScoreMenu = GetComponent<AnimateScoreMenu> ();

		sliderTeamOne = atrasTeamOne.transform.Find ("Filled").GetComponent<Image>();
		textTeamOne = atrasTeamOne.transform.Find ("Text").GetComponent<Text>();
		sliderTeamTwo = atrasTeamTwo.transform.Find ("Filled").GetComponent<Image>();
		textTeamTwo = atrasTeamTwo.transform.Find ("Text").GetComponent<Text>();

		sliderPlayerOne = atrasPlayerOne.transform.Find ("Filled").GetComponent<Image>();
		textPlayerOne = atrasPlayerOne.transform.Find ("Text").GetComponent<Text>();
		sliderPlayerTwo = atrasPlayerTwo.transform.Find ("Filled").GetComponent<Image>();
		textPlayerTwo = atrasPlayerTwo.transform.Find ("Text").GetComponent<Text>();
		sliderPlayerThree = atrasPlayerThree.transform.Find ("Filled").GetComponent<Image>();
		textPlayerThree = atrasPlayerThree.transform.Find ("Text").GetComponent<Text>();

		activateTeams = false;
		activatePlayers = false;

		currentValueOne = 0.0f;
		currentValueTwo = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if (activateTeams) {

			bool incrOne = IncreaseValue (sliderTeamOne, textTeamOne, ref valueOne, ref currentValueOne);

			bool incrTwo = IncreaseValue (sliderTeamTwo, textTeamTwo, ref valueTwo, ref currentValueTwo);

			if (incrTwo && incrOne) {
			
				activateTeams = false;
			
				animScoreMenu.StopSecondCircle ();
			}
		}

		if (activatePlayers) {
		
			bool incrOne = IncreaseValue (sliderPlayerOne, textPlayerOne, ref playerValueOne, ref currentPlayerOne);

			bool incrTwo = IncreaseValue (sliderPlayerTwo, textPlayerTwo, ref playerValueTwo, ref currentPlayerTwo);

			bool incrThree = IncreaseValue (sliderPlayerThree, textPlayerThree, ref playerValueThree, ref currentPlayerThree);

			if (incrTwo && incrOne && incrThree)
				activatePlayers = false;
			
		}
	}

	private bool IncreaseValue(Image slider, Text text, ref float maxValue, ref float currVal)
	{
		if (currVal >= maxValue)
			return true;
		
		currVal += 0.005f;

		slider.fillAmount = currVal;

		int roundV;
		if (activateTeams)
			roundV = 100;
		else
			roundV = 10;

		text.text = (int)(currVal * roundV) + "";

		if (!activatePlayers)
			text.text += "%";

		if (currVal >= maxValue)
			return true;

		return false;
	}
}
