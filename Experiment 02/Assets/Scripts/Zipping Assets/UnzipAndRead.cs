﻿using UnityEngine;
using System.Collections;
using System.IO;

public class UnzipAndRead : MonoBehaviour {

	private MeshRenderer myRend;

	void Start()
	{
		myRend = GetComponent<MeshRenderer> ();
	}

	void OnGUI()
	{
		if( GUI.Button(new Rect(0, 0, 300, 300), "load") )
		{
			StartCoroutine(Load());
		}
	}

	IEnumerator Load()
	{
		// Path di salvataggio del file .Zip appena scaricato
		string zipPath = Application.temporaryCachePath + "/tempZip.zip";
		// Cartella di supporto in cui apre l'archivio appena scaricato
		string exportPath = Application.temporaryCachePath + "/unzip";
		// File da utilizzare tra quelli scaricati, il nome deve coincidere con quello presente nell'archivio
		string imagePath = exportPath + "/S3_bias_corrected00000189.png";

		Debug.Log (exportPath);

		WWW www = new WWW("http://localhost:8080/MindCraft/LevelsController/zipDownload");

		//yield return www;
		while (!www.isDone) {
			Debug.Log("downloaded " + www.progress + " %...");
			yield return null;
		}

		var data = www.bytes;
		File.WriteAllBytes(zipPath, data);
		ZipUtil.Unzip(zipPath, exportPath);

		var tex =  new Texture2D(1, 1);

	 	var imageData = File.ReadAllBytes(imagePath);
		tex.LoadImage(imageData);

		/*
		GetComponent<UnityEngine.UI.RawImage>().texture = tex;
		*/
		myRend.material.EnableKeyword ("_DETAIL_MULX2");
		myRend.material.SetTexture("_DetailAlbedoMap", tex);
		myRend.material.SetTexture("_MainTex", tex);

		// Eliminare file zip appena scaricato
		File.Delete(zipPath);
		// Eliminare cartella coi file appena scaricati, perchè in teoria li ho già utilizzati nel mio progetto,
		// in pratica non lo so
		Directory.Delete(exportPath, true);
	
		Debug.Log ("Done");
	}
}
