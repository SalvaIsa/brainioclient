﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class XRayEffect : MonoBehaviour {

	public Transform player;

	private TargetParameters parameters;

	void Start () {

		parameters = player.GetComponent<TargetParameters> ();
	}
	
	void Update () {

//		if (!parameters.GetTimer ().over) {
		if (parameters.ZawarudoStepTwo) {

			if (!parameters.TweenDone) {
			
				DOTween.To(()=> parameters.CurrentRadius, x=> parameters.CurrentRadius = x, parameters.MaxRadius, 0.3f);
				parameters.TweenDone = true;
			}
				
			GetComponent<Renderer> ().material.SetFloat ("_Radius", parameters.CurrentRadius);

			GetComponent<Renderer> ().material.SetVector ("_ObjPos", new Vector4 (player.position.x, player.position.y, player.position.z, 0));
		}
		else {

			if (!parameters.TweenDone) {

				DOTween.To(()=> parameters.CurrentRadius, x=> parameters.CurrentRadius = x, parameters.MinRadius, 0.6f);
				parameters.TweenDone = true;
			}

			GetComponent<Renderer> ().material.SetFloat ("_Radius", parameters.CurrentRadius);

			GetComponent<Renderer> ().material.SetVector ("_ObjPos", new Vector4 (player.position.x, player.position.y, player.position.z, 0));
		}
	}
}
