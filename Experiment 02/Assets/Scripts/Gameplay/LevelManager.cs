﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public PanningZoomingCamera gameCamera;
	public IntTimer gameTimer;
	public Transform gameLayers;
	public Slider sliceSlider;
	
	public List<string> selectedDamges;
	public Transform debugObj;
	public bool debugMode;

	private List<string>[] damagesArray;

	private LevelBuilder builder;
	private int currentSlice;	// serve per iterare tra tutti i file, parte da 1 
	private int slicesAmount;

	private SpawningPlayer spawnPlayer;
	private ScoringHandler scoringHandler;
	private GuiBoxesMaker boxesMaker;
	private GameSceneManagement gameSceneManage;
	private PartitionedTextureClicking partTextClick;

	void Start () {
		
		selectedDamges = new List<string> ();
		builder = GetComponent<LevelBuilder> ();
		boxesMaker = GetComponent<GuiBoxesMaker> ();
		scoringHandler = GetComponent<ScoringHandler> ();
		spawnPlayer = GetComponent<SpawningPlayer> ();
		gameSceneManage = GetComponent<GameSceneManagement> ();
		partTextClick = GetComponent<PartitionedTextureClicking> ();

		currentSlice = 1;

		if (!debugMode && !ApplicationModel.offlineMode) {

			ApplicationModel.sliceClusters = new Dictionary<Color, int>[ApplicationModel.slicesCount];
			ApplicationModel.sliceValidities = new float[ApplicationModel.slicesCount];

			builder.buildLevelFromTexture ();

			slicesAmount = ApplicationModel.slicesCount;

			damagesArray = new List<string>[slicesAmount];
			for (int i = 0; i < damagesArray.Length; i++) {
			
				LayerParameters lay = gameLayers.Find ("Layers " + (i+1)).GetComponent<LayerParameters> ();

				damagesArray[i] = new List<string> ();
				damagesArray [i].Add (lay.GetSliceNumber () + ">>>");
			}

			sliceSlider.maxValue = slicesAmount;
			sliceSlider.value = 1;

		} 
		else {

			Debug.Log ("GNEGNA");

			damagesArray = new List<string>[8];
			for(int i = 0; i < damagesArray.Length; i++)
				damagesArray[i] = new List<string> ();

			sliceSlider.maxValue = 8;
			sliceSlider.value = 1;
		}

	}

	void Update()
	{
		if (gameTimer.IsTimerOver ()) {
		
			ComputeSelectionIndex ();

			if (IsGameOver ()) {

				GameOverProcedure ();

			} else {

				if (gameCamera.IsMovingCompleted ()) {

					gameCamera.MoveToNextSlice ();

					IncreaseCurrentSlice ();
					boxesMaker.UpdateTextureParameters (GetCurrentSlice ());

					partTextClick.MovePartitionedPlane ();
					partTextClick.textPartition = boxesMaker.textPartition;

					gameTimer.ResetTimer (GetCurrentSlice());
				}
			}
		}
	}

	public void GameOverProcedure()
	{
		Debug.Log ("Sending Selections...");

		ApplicationModel.modelDamagesArray = damagesArray; //GetWholeDamages();

		LoadingScreenManager.LoadScene (3);
	}

	public void AddToSelectedDamage(string pixels)
	{
		damagesArray [currentSlice-1].Add (pixels);
	}

	public void IncreasePixelCluster(Color clusterCol)
	{
		ApplicationModel.sliceClusters [currentSlice - 1] [clusterCol]++;
	}

	public void ComputeSelectionIndex()
	{
		int totalPixelNumber = 0;
		int maxPixelNum = 0;

		foreach(Color key in ApplicationModel.sliceClusters [currentSlice - 1].Keys)
		{
			int pixels = ApplicationModel.sliceClusters [currentSlice - 1] [key];

			totalPixelNumber += pixels;

			if (pixels > maxPixelNum)
				maxPixelNum = pixels;
		}

		float validity;

		if (totalPixelNumber == 0.0f)
			validity = 1.0f;
		else
			validity = (float)maxPixelNum / (float)totalPixelNumber;

		ApplicationModel.sliceValidities [currentSlice - 1] = validity;

		float avgSci = 0.0f;
		for (int i = 0; i < currentSlice; i++) {
		
			avgSci += ApplicationModel.sliceValidities [i];
		}

		avgSci = avgSci / (float)currentSlice;

		gameSceneManage.FillSciIndex (avgSci);

		Debug.Log ("Total Pixels = " + totalPixelNumber);
		Debug.Log ("Max Pixel Number = " + maxPixelNum);
		Debug.Log ("Slice " + currentSlice + " index = " + validity);
		Debug.Log ("AVG = " + avgSci);
	}
		
	public bool IsGameOver()
	{
		if (currentSlice == slicesAmount)
			return true;

		return false;
	}

	public void IncreaseCurrentSlice()
	{
		currentSlice++;
		sliceSlider.value+=1;
	}

	public int GetCurrentSlice()
	{
		return currentSlice;
	}

	public void SetCurrentSlice(int val)
	{
		Debug.Log ("Nuova slice = " + val);

		currentSlice = val;
	}


	public List<string> GetDamagesList(int position)
	{
		return damagesArray [position];
	}

	public List<string>[] GetWholeDamages()
	{
		return damagesArray;
	}

}
