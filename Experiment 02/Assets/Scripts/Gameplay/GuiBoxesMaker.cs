﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiBoxesMaker : MonoBehaviour {

	public Camera cam;
	public PartitionedTextureClicking textClicking;
	public TextureRecoloring textRecolor;
	public TexturePartition textPartition;
	public float boxMinSize;
	public float boxMaxWidth;
	public bool drawBoxes;
	public Texture2D boxTexture;
	public LayerMask textureLayer;
	public Transform gameLayers;

	private static Vector3 mouseDownPoint;
	private static Vector3 mouseUpPoint;
	private static Vector3 currentMousePoint;

	private float boxWidth;
	private float boxHeight;
	private Vector2 boxStart;
	private Vector2 boxFinish;

	private bool UserIsDragging;
	private Vector2 MouseDragStart;

	private PanningZoomingCamera panZoomComponent;
	private SpawningPlayer spawningPlayer;
	private GameSceneManagement sceneManagement;
	private List<SelectionBox> selectedBoxes = new List<SelectionBox>();
	private SelectionBox currentBox = null;
	private bool correctBox;

	void Start()
	{
		panZoomComponent = cam.GetComponent<PanningZoomingCamera> ();
		spawningPlayer = GetComponent<SpawningPlayer> ();
		sceneManagement = GetComponent<GameSceneManagement> ();
		correctBox = true;
	}

	void Update () {

		if (spawningPlayer.IsZaWarudoStepOne ()) {
			
			textRecolor.SetZawaBackground ();
		} else
			textRecolor.SetNormalBackground ();

		if (textClicking.selectionEnabled || spawningPlayer.IsDrawingStepOne() || spawningPlayer.IsZaWarudoStepOne ())
			return;


		if (SpawningPlayer.IsPointerOverUIObject () || panZoomComponent.IsZoomingIn())
			return;

		//DraggingBuildBox ();
	}

	void OnGUI()
	{
		if (textClicking.selectionEnabled || spawningPlayer.IsDrawingStepOne())
			return;

		if(drawBoxes)
			foreach (SelectionBox box in selectedBoxes) {

				GUI.color = new Color (0.0f, 0.0f, 255.0f, 0.3f);
				GUI.depth = -1000;

				GUI.DrawTexture (new Rect (box.onguiX, box.onguiY, box.onguiWidth, box.onguiHeight), boxTexture);
			}

		if (UserIsDragging) {

			if(correctBox)
				GUI.color = new Color (255.0f, 255.0f, 255.0f, 0.3f);
			else
				GUI.color = new Color (255.0f, 0.0f, 0.0f, 0.3f);
			
			GUI.depth = -1000;

			var x = Camera.main.WorldToScreenPoint (mouseDownPoint).x;
			var y = Camera.main.WorldToScreenPoint (mouseDownPoint).y;

			GUI.DrawTexture (new Rect (x, Screen.height - y, boxWidth, boxHeight), boxTexture);
		}
	}

	void DraggingBuildBox()
	{
		if (Input.GetMouseButton (0)) {
			
			RaycastHit hit;
			Ray ray = cam.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit, 1000f, textureLayer)) {
				
				if (!UserIsDragging) {

					Vector2 startingPixText = GetTexturePixel (hit);

					UserIsDragging = true;
					mouseDownPoint = hit.point;
			
					currentBox = new SelectionBox ();

					currentBox.onguiX = Camera.main.WorldToScreenPoint (mouseDownPoint).x;
					currentBox.onguiY = Screen.height - Camera.main.WorldToScreenPoint (mouseDownPoint).y;

					currentBox.cameraX = (int)mouseDownPoint.x;
					currentBox.cameraY = (int)mouseDownPoint.y;

					currentBox.AtapTextureX = (int)startingPixText.x;
					currentBox.AtapTextureY = (int)startingPixText.y;
				}

				currentMousePoint = hit.point;

				currentBox.endCameraX = (int)currentMousePoint.x;
				currentBox.endCameraY = (int)currentMousePoint.y;

				boxWidth = Camera.main.WorldToScreenPoint (currentMousePoint).x - Camera.main.WorldToScreenPoint (mouseDownPoint).x; 
				boxHeight = Camera.main.WorldToScreenPoint (mouseDownPoint).y - Camera.main.WorldToScreenPoint (currentMousePoint).y;

				if (currentBox != null) {

					currentBox.onguiWidth = boxWidth;
					currentBox.onguiHeight = boxHeight;

					if (currentBox.onguiWidth == 0 || currentBox.onguiHeight == 0 
						|| Mathf.Abs(currentBox.onguiWidth) >= boxMaxWidth || Mathf.Abs(currentBox.onguiHeight) >= boxMaxWidth
						|| Mathf.Abs(currentBox.onguiWidth) <= boxMinSize || Mathf.Abs(currentBox.onguiHeight) <= boxMinSize)
						correctBox = false;
					else
						correctBox = true;
					
					Vector2 currentPixText = GetTexturePixel (hit);

					currentBox.DtapTextureX = (int)currentPixText.x;
					currentBox.DtapTextureY = (int)currentPixText.y;
				}
			}
		} else {

			if (currentBox != null) {

				if (correctBox) {

					currentBox.FindBoxCenter ();

					sceneManagement.MoveZoomCamera (new Vector3 (currentBox.centerCamX, currentBox.centerCamY, cam.transform.position.z), 
						panZoomComponent.GetZoomingFov());

					panZoomComponent.SetZoomingIn (true);

//					selectedBoxes.Add (currentBox);
//					textPartition.SetPartitionParameter (currentBox);
//					textClicking.selectionEnabled = true;
				}

				currentBox = null;
			}

			UserIsDragging = false;

		}
	}

	private Vector2 GetTexturePixel(RaycastHit planeHit)
	{
		Renderer rend = planeHit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = planeHit.collider as MeshCollider;

		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
			return new Vector2 (-1, -1);

		Texture2D tex = rend.material.mainTexture as Texture2D;

		Vector2 pixelUV = planeHit.textureCoord;

		pixelUV.x *= tex.width;
		pixelUV.y *= tex.height;

		return pixelUV;
	}

	public void UpdateTextureParameters(int currentSlice)
	{
		MeshRenderer biasLayer = gameLayers.Find ("Layers " + currentSlice).Find ("BiasLayer").GetComponent<MeshRenderer>();

		textPartition = biasLayer.GetComponent<TexturePartition> ();
		textRecolor = biasLayer.GetComponent<TextureRecoloring> ();

	}

	public TexturePartition GetCurrentTexturePartition()
	{
		return textPartition;
	}
}
