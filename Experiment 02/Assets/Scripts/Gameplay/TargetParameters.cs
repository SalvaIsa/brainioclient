﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetParameters : MonoBehaviour {

	public float MaxRadius;
	public float MinRadius;
	public float CurrentRadius;
	public bool ZawarudoStepTwo;
	public bool TweenDone;

	[SerializeField] FloatTimerZawarudo timer;

	void Start()
	{
		ZawarudoStepTwo = false;
		TweenDone = false;
		CurrentRadius = MinRadius;
	}

	public FloatTimerZawarudo GetTimer()
	{
		return timer;
	}

	public void SwitchZaWarudoStepTwo(bool value)
	{
		ZawarudoStepTwo = value;
		TweenDone = false;
	}

	public void ResetCurrentRadius()
	{
		CurrentRadius = MinRadius;
	}

}
