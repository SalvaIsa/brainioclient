﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class LevelBuilder : MonoBehaviour {

	public Transform gameLayers;
	public MeshRenderer biasRenderer;
	public MeshRenderer visorRenderer;

	public IntTimer gameTimer;

	public bool debugMode;

	void Start () {

		if (!debugMode && !ApplicationModel.offlineMode)
			Debug.Log ("Slices totali " + ApplicationModel.slicesCount);
		
	}
		
	public void buildLevelFromTexture()
	{
		if (ApplicationModel.biasTextures == null)
			return;

		char[] seps = new char[1];
		seps [0] = '(';

		for (int i = 1; i <= ApplicationModel.biasTextures.Length; i++) {
				
			MeshRenderer biasLayer = gameLayers.Find ("Layers " + i).Find ("BiasLayer").GetComponent<MeshRenderer>();
			MeshRenderer visorLayer = gameLayers.Find ("Layers " + i).Find ("VisorLayer").GetComponent<MeshRenderer>();

			Texture2D biasTex =  ApplicationModel.biasTextures[i-1] as Texture2D;
			Texture2D visorTex =  ApplicationModel.visorTextures[i-1] as Texture2D;
			Texture2D clusteredTex =  ApplicationModel.clusteredTextures[i-1] as Texture2D;
			Texture2D superpixelTex =  ApplicationModel.superpixelTextures[i-1] as Texture2D;

			Texture2D readableBiasTex = GetReadableTexture (biasTex);
			Texture2D readableVisorTex = GetReadableTexture (visorTex);
			Texture2D readableClusteredTex = GetReadableTexture (clusteredTex);
			Texture2D readableSuperpixelTex = GetReadableTexture (superpixelTex);

			readableBiasTex.name = biasTex.name + " readable";
			readableVisorTex.name = visorTex.name + " readable";

			biasLayer.material.EnableKeyword ("_DETAIL_MULX2");
			biasLayer.material.SetTexture("_MainTex", readableBiasTex);
			
			visorLayer.material.EnableKeyword ("_DETAIL_MULX2");
			visorLayer.material.SetTexture("_MainTex", readableVisorTex);
			visorLayer.material.SetTexture("_DetailAlbedoMap", readableVisorTex);

			string[] pcs = biasTex.name.Split (seps, 2);
			int sliceNumber = int.Parse (pcs [1].Replace (")", ""));

			Transform layerParent = gameLayers.Find ("Layers " + i);

			LayerParameters lay = layerParent.GetComponent<LayerParameters> ();
			lay.SetSliceNumber (sliceNumber);
			lay.SetClusteredTexture (readableClusteredTex);
			lay.SetSuperPixelTexture (readableSuperpixelTex);

			TextureRecoloring recolor = biasLayer.GetComponent<TextureRecoloring> ();
			recolor.SetCurrentIndex (i);
			recolor.SetVisorRenderer (visorLayer);
			recolor.SetNormalBackground ();

			layerParent.Find ("OverlayLayer").GetComponent<MeshRenderer> ().material.mainTexture = ApplicationModel.overlayTextures [i - 1] as Texture2D;

//			TexturePartition txtPart = biasLayer.GetComponent<TexturePartition> ();
//			if (txtPart != null) {
//
//				txtPart.SetSourcePlanes (visorLayer, biasLayer);
//				txtPart.CountClustersInTexture (i-1);
//			}
		}
	}

	public Texture2D GetReadableTexture(Texture2D texture)
	{
		// Create a temporary RenderTexture of the same size as the texture
		RenderTexture tmp = RenderTexture.GetTemporary( 
			texture.width,
			texture.height,
			0,
			RenderTextureFormat.Default,
			RenderTextureReadWrite.sRGB); //RenderTextureReadWrite.Linear);

		// Blit the pixels on texture to the RenderTexture
		Graphics.Blit(texture, tmp);

		// Backup the currently set RenderTexture
		RenderTexture previous = RenderTexture.active;

		// Set the current RenderTexture to the temporary one we created
		RenderTexture.active = tmp;

		// Create a new readable Texture2D to copy the pixels to it
		Texture2D myTexture2D = new Texture2D(texture.width, texture.height);

		// Copy the pixels from the RenderTexture to the new Texture
		myTexture2D.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
		myTexture2D.Apply();

		// Reset the active RenderTexture
		RenderTexture.active = previous;

		// Release the temporary RenderTexture
		RenderTexture.ReleaseTemporary(tmp);

		// "myTexture2D" now has the same pixels from "texture" and it's readable.
		return myTexture2D;
	}

	//		string imagePath = ApplicationModel.slicePath + "(" + currSlice + ").png";

	//		Texture2D tex =  new Texture2D(1, 1);			
	//		var imageData = File.ReadAllBytes(imagePath);
	//		tex.LoadImage(imageData);
}
