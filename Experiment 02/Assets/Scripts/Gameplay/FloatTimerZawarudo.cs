﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloatTimerZawarudo: MonoBehaviour {

	public Text dbgText;

	[SerializeField] GameSceneManagement sceneManagement;

	public Button zawarudoButton;
	public TargetParameters targetParameters;

	public bool over;

	public float Max_minutes;
	public float Max_seconds;
	public float Max_milliseconds;

	private float minutes;
	private float seconds;
	private float milliseconds;

	private float Upper_Seconds_Limit;
	private float increasingMaxSeconds = 0f;
	private float fillingSeconds;

	void Start()
	{
		over = true;

		minutes = Max_minutes;
		seconds = Max_seconds;
		milliseconds = Max_milliseconds;
	
		Upper_Seconds_Limit = Max_seconds;

		fillingSeconds = 0.0f;

		zawarudoButton.GetComponentInChildren<Text> ().text = Max_seconds + "";
	}

	private void FillImageInTime()
	{
		fillingSeconds += Time.deltaTime;

		float fillAmount = fillingSeconds / Max_seconds;

		sceneManagement.FillZawarudoImage (fillAmount);
	}

	void Update(){

		if (!over) {

			FillImageInTime ();

			zawarudoButton.GetComponentInChildren<Text> ().text = seconds + "";

			if (milliseconds <= 0) {
				if (seconds <= 0) {
					
					if (minutes <= 0) {
						
						StopTimer ();

						return;
					}

					minutes--;
					seconds = 59;
				} else if (seconds >= 0) {
					seconds--;
				}

				milliseconds = 100;
			}

			milliseconds -= Time.deltaTime * 100;

			//		timerText.text = string.Format ("{0}:{1}:{2}", minutes, seconds, (int)milliseconds);
		
		} else {
			
			increasingMaxSeconds += Time.deltaTime;
			int secco = (int)Mathf.Round (increasingMaxSeconds);

			//		dbgText.text = (Max_seconds + secco <= Upper_Seconds_Limit) + "";
			//		dbgText.text += "  " + Max_seconds + " vs " + Upper_Seconds_Limit;

			if (secco == 1) {

				if (Max_seconds + secco <= Upper_Seconds_Limit) {

					Max_seconds += secco;
					zawarudoButton.GetComponentInChildren<Text> ().text = Max_seconds + "";
				
				}
					increasingMaxSeconds = 0f;
			}
		}
	}

	public void ResetTimer()
	{
		over = false;

		minutes = Max_minutes;
		seconds = Max_seconds;
		milliseconds = Max_milliseconds;
	
		fillingSeconds = 0.0f;
		sceneManagement.FillZawarudoImage(0.0f);
	}

	public void StopTimer()
	{
		over = true;

		Max_seconds = seconds;
		targetParameters.ZawarudoStepTwo = false;

		sceneManagement.FillZawarudoImage(1.0f);
		sceneManagement.EnlargeZawarudoImage ();
	}

}