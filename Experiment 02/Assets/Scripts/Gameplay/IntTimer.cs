﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntTimer : MonoBehaviour
{
	[SerializeField] GameSceneManagement sceneManagement;
	[SerializeField] float maxTimeLeft;

	public Text text;

	private float[] timesLeftArray;

	private float timeLeft;
	private float fillingSeconds;
	private bool enableStart = false;
	private bool over = false;

	void Start()
	{
		//	timeLeft = maxTimeLeft;
		fillingSeconds = 0.0f;

		timesLeftArray = new float[ApplicationModel.biasTextures.Length];
	}

	void Update()
	{
		if (!over && enableStart) {

			FillImageInTime ();

			timeLeft -= Time.deltaTime;
		//	text.text = Mathf.Round(timeLeft) + "";

			if(timeLeft < 0)
			{
				over = true;
			}
		}
	}

	private void FillImageInTime()
	{
		fillingSeconds += Time.deltaTime;

		float fillAmount = fillingSeconds / maxTimeLeft;

		sceneManagement.FillTimerImage(1 - fillAmount);
	}

	public bool IsTimerOver()
	{
		return over;
	}

	public void SetMaxTimeAt(int currSlice, int nonWhites)
	{
		Debug.Log ("At position " + currSlice + " nW = " + nonWhites);

		if (nonWhites < 10000) {
			timesLeftArray [currSlice-1] = 900.0f;
		} else if (nonWhites < 19000) {
			timesLeftArray [currSlice-1] = 28.0f;
		}
		else
			timesLeftArray [currSlice-1] = 35.0f;
	}

	public void EnableStart()
	{
		maxTimeLeft = timesLeftArray [0];
		timeLeft = maxTimeLeft;

		enableStart = true;
	}

	public void ResetTimer(int currSlice)
	{
		maxTimeLeft = timesLeftArray [currSlice-1];

		timeLeft = maxTimeLeft;
		fillingSeconds = 0.0f;
		over = false;
	}
}
