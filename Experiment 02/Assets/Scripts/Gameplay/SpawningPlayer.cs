﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpawningPlayer : MonoBehaviour {

	public Camera cam;
	public Transform zawarudoTarget;
	public Transform drawingTarget;
	public float offsetTarget;
	public float zawarudoOffsetTarget;

	public Text dbgText;

	private PanningZoomingCamera cameraPanZoom;
	private TargetParameters zawarudoParameters;
	private PartitionedTextureClicking partTextClick;

	private bool zawarudoStepOne;
	private bool drawingStepOne;

	private Color currentDraggingColor;

	private LevelManager manager;
	private GuiBoxesMaker boxesMaker;

	// For double tap detection
	private float myTouchDur;
	private bool myBoolDoubletap;

	private bool myFirstTap;
	private bool mySecondTap;

	private float touchDuration;
	private Touch touch;

	void Start () {
	
		cameraPanZoom = cam.GetComponent<PanningZoomingCamera> ();
		zawarudoParameters = zawarudoTarget.GetComponent<TargetParameters>();
		partTextClick = GetComponent<PartitionedTextureClicking> ();

		zawarudoStepOne = false;
		drawingStepOne = false;

		manager = GetComponent<LevelManager> ();
		boxesMaker = GetComponent<GuiBoxesMaker> ();

		currentDraggingColor = PartitionedTextureClicking.nullColor;

	}

	void Update() {

		if (myFirstTap) {
			myTouchDur += Time.deltaTime;
		
			if (myTouchDur > 0.5f) {
			
				myFirstTap = false;
				myTouchDur = 0.0f;
			}
		}
			
		if (!Input.GetMouseButton(0))
			return;
			
		if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {

			HandleTouch ();

			if (!IsPointerOverUIObject ()) {}

		} else {
		
			StandardHandling (false, new Vector2(Input.mousePosition.x, Input.mousePosition.y));

			if (!IsPointerOverUIObject ()) {}
		}

	}

	private void HandleTouch() {

		switch(Input.touchCount) {

		case 1: 

			Touch touch = Input.GetTouch (0);
			Debug.Log ("fID = " + touch.fingerId + " fPos = " + touch.position);
			//dbgText.text = "fID = " + touch.fingerId + " fPos = " + touch.position;

			StandardHandling (true, touch.position);

			break;
		
		case 2:

			Touch touch2 = Input.GetTouch (0);
			Debug.Log ("fID = " + touch2.fingerId + " fPos = " + touch2.position);
			//	dbgText.text = "fID = " + touch2.fingerId + " fPos = " + touch2.position;

			Touch touch3 = Input.GetTouch (1);
			Debug.Log ("fID = " + touch3.fingerId + " fPos = " + touch3.position);
			//	dbgText.text += "\nfID = " + touch3.fingerId + " fPos = " + touch3.position;

			StandardHandling (true, touch2.position);

			break;

		default:	
			break;
		}


		//	else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Began){
		//	case 2:
		//	Vector2[] newPositions = new Vector2[]{Input.GetTouch(0).position, Input.GetTouch(1).position};
	}

	private void StandardHandling(bool touchActive, Vector2 XYposition)
	{			
		if (zawarudoStepOne)
			ZaWarudoDraggingMode (XYposition);
		//	ZaWarudoMode (touchActive);

		if (drawingStepOne)
			DrawingDraggingMode (XYposition);
	
	}
				
	private void ZaWarudoDraggingMode(Vector2 XYpos)
	{		
		Vector3 offsetPosition = new Vector3 (XYpos.x, XYpos.y + zawarudoOffsetTarget, Input.mousePosition.z);

		RaycastHit[] hits;
		hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(offsetPosition), 100.0F);

		for (int i = 0; i < hits.Length; i++) {

			RaycastHit hit = hits [i];
			if (hit.collider.tag == "BiasPlane") {

				Vector3 newPos = new Vector3 (hit.point.x, hit.point.y, zawarudoTarget.position.z);
				zawarudoTarget.position = newPos;

				Renderer rend = hit.transform.GetComponent<Renderer>();
				MeshCollider meshCollider = hit.collider as MeshCollider;

				if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
					return;

				Texture2D tex = rend.material.mainTexture as Texture2D;

				Vector2 pixelUV = hit.textureCoord;

				pixelUV.x *= tex.width;
				pixelUV.y *= tex.height;

				currentDraggingColor = tex.GetPixel ((int)pixelUV.x, (int)pixelUV.y);

				if(IsBackgroundPixel())
					zawarudoParameters.SwitchZaWarudoStepTwo(false);
				else
					zawarudoParameters.SwitchZaWarudoStepTwo(true);
				
			}
		}
	}

	private void ZaWarudoMode(bool touchActive)
	{
		if (zawarudoParameters.ZawarudoStepTwo)
			return;
		
		RaycastHit hit;
		if (!Physics.Raycast (cam.ScreenPointToRay (Input.mousePosition), out hit))
			return;

		if (hit.collider != null) {
			
			if (hit.collider.tag == "Player") {
						
				if (touchActive && Input.GetTouch (0).phase == TouchPhase.Began) {

					if (!myFirstTap)
						myFirstTap = true;
					else if (myFirstTap) {	// Double Tap Detected

						myFirstTap = false;
						myTouchDur = 0.0f;
					
						zawarudoParameters.GetTimer ().ResetTimer ();
						zawarudoParameters.ZawarudoStepTwo = true;
						zawarudoStepOne = false;
						zawarudoTarget.GetComponent<GifComponent> ().SetVisible (false);

						//	SetSelectionStepOne ();
					}
				} else if (!touchActive) {

					zawarudoParameters.GetTimer ().ResetTimer ();
					zawarudoParameters.ZawarudoStepTwo = true;
					zawarudoStepOne = false;
					zawarudoTarget.GetComponent<GifComponent> ().SetVisible (false);

					//	SetSelectionStepOne ();
				}
			} else {

				//zawarudoParameters.ResetCurrentRadius ();

				Vector3 newPos = new Vector3 (hit.point.x, hit.point.y, zawarudoTarget.position.z);
				zawarudoTarget.position = newPos;

			}
		
		}
	}

	private void DrawingDraggingMode(Vector2 XYpos)
	{
		Vector3 offsetPosition = new Vector3 (XYpos.x, XYpos.y + offsetTarget, Input.mousePosition.z);

		RaycastHit[] hits;
		hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(offsetPosition), 100.0F);

		for (int i = 0; i < hits.Length; i++) {

			RaycastHit hit = hits [i];
			if (hit.collider.tag == "BiasPlane") {

				Vector3 newPos = new Vector3 (hit.point.x, hit.point.y, drawingTarget.position.z);
				drawingTarget.position = newPos;

				Renderer rend = hit.transform.GetComponent<Renderer>();
				MeshCollider meshCollider = hit.collider as MeshCollider;

				if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
					return;

				Texture2D tex = rend.material.mainTexture as Texture2D;

				Vector2 pixelUV = hit.textureCoord;

				pixelUV.x *= tex.width;
				pixelUV.y *= tex.height;

				currentDraggingColor = tex.GetPixel ((int)pixelUV.x, (int)pixelUV.y);

				if (IsBackgroundPixel ()) {
				
					partTextClick.ResettingPlaneTextures ();

				} else {

					partTextClick.StartColoringProcedure (false, XYpos);		
				}
				
			}
		}
	}
		
	public bool IsBackgroundPixel()
	{
		return currentDraggingColor.a == 0.0f;
	}

	public void ResetCurrentDraggingColor()
	{
		currentDraggingColor = PartitionedTextureClicking.nullColor;
	}

	public void SetZaWarudoStepOne()
	{
		zawarudoStepOne = !zawarudoStepOne;
		zawarudoTarget.GetComponent<GifComponent> ().SetVisible (zawarudoStepOne);
	}
	
	public void SetZaWarudoStepTwo()
	{
		zawarudoParameters.ZawarudoStepTwo = false;
		SetZaWarudoStepOne ();
	}

	public bool IsZaWarudoStepOne()
	{
		return zawarudoStepOne;
	}

	public bool IsZaWarudoStepTwo()
	{
		return zawarudoParameters.ZawarudoStepTwo;
	}

	public void SetDrawingStepOne()
	{
		drawingStepOne = !drawingStepOne;
		drawingTarget.GetComponent<GifComponent> ().SetVisible (drawingStepOne);
	}

	public bool IsDrawingStepOne()
	{
		return drawingStepOne;
	}

	public static bool IsPointerOverUIObject() {
		
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
	
		return results.Count > 0;
	}
}